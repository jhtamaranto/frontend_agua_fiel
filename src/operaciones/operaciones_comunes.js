const OPERACIONES_COMUNES={
    calcularIgv(){

    },
    contabilizarProductosEnCarro(productos) {
        let quantity = 0
        productos.forEach(element => {
            quantity = quantity + parseInt(element.quantity)
        });
        return quantity
    },

    calculateSubTotalPrice(cart) {
        let subtotal = 0
        cart.forEach(element => {
            subtotal = subtotal + element.price
        });

        return subtotal
    },
    calculatePriceFromProductItem(product){
        let base_price = product.base_price
        let quantity = product.quantity
        let subtotalItem = base_price * quantity
        let discount = 0
        /*
        if (product.discount_type === 'Monto') {
            discount = product.discount_amount
        } else if(product.discount_type === 'Porcentaje') {
            discount = (product.discount_amount / 100) * product.price
        }
        let totalItem = subtotalItem - discount
        */
        let totalItem = 0;
        if (product.discount_group == true) {
            discount = this.calculateDiscountValueToPriceUnitItem(product);

            totalItem = subtotalItem - ( discount * quantity);
        } else {
            discount = this.calculateDiscountValueToSubtotalItem(product);
            totalItem = subtotalItem - discount;
        }

        return totalItem
    },
    calculateSubTotal(total, por_igv){
        let subtotal = total/(1 + por_igv)
        subtotal = Math.round(subtotal * 100) / 100;

        return subtotal
    },
    findIndexFromItem(data, idSearched) {
        let indexFounded = -1
        data.forEach( (element, index) => {
            if (element.id === idSearched) {
                indexFounded = index
            }
        })

        return indexFounded
    },
    calculateDiscountValueToSubtotalItem (product) {
        let base_price = parseFloat(product.base_price)
        let quantity = parseFloat(product.quantity)
        let subtotalItem = parseFloat(base_price * quantity)
        let discount_amount = parseFloat(product.discount_amount)

        let discount_value = 0
        if (product.discount_type === 'Monto') {
            discount_value = discount_amount
        } else if(product.discount_type === 'Porcentaje') {
            discount_value = (discount_amount / 100) * subtotalItem;
        }

        return discount_value;
    },
    calculateDiscountValueToPriceUnitItem (product) {
        let base_price = parseFloat(product.base_price);
        let quantity = parseFloat(product.quantity);
        let discount_amount = parseFloat(product.discount_amount);

        let discount_value = 0;
        if (product.discount_type === 'Monto') {
            discount_value = discount_amount;
        } else if(product.discount_type === 'Porcentaje') {
            discount_value = (discount_amount / 100) * base_price;
        }

        return discount_value;

    },
    calculatCartAmounts (cart) {
        let subtotal = 0;
        let total_bonus = 0;
        for (let element of cart) {
            if (element.is_bonus == true) {
                total_bonus = total_bonus + element.price;
            } else {
                subtotal = subtotal + element.price;
            }
        }

        return {
            subtotal: Math.round(subtotal * 100) / 100,
            total_bonus: Math.round(total_bonus * 100) / 100
        }
    }
}
export default  OPERACIONES_COMUNES