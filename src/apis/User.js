import Api from './Api'

const END_POINT = 'users'

export default {
    getAll() {
        return Api.get(`${END_POINT}/get-all`)
    },
    getRoles() {
        return Api.get(`${END_POINT}/get-roles`)
    },
    create(data) {
        return Api.post(`${END_POINT}/create`, data);
    },
    update(data) {
        return Api.post(`${END_POINT}/update`, data);
    },
    changeStatus(id) {
        return Api.delete(`${END_POINT}/change-status/${id}`);
    },
    getAvailableDeliveryUsers(){
        return Api.get(`${END_POINT}/available-delivery-users`)
    }
}