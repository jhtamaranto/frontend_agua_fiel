import Api from './Api'

const END_POINT = 'invoices';

export default {
    create(data) {
        return Api.post(`${END_POINT}/generate`, data);
    },
}