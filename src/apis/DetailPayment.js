import Api from './Api';

const END_POINT = 'detail-payments';

export default {
    async get_all(filters = null) {
        let request = {
            params: filters
        }
        return await Api.get(`${END_POINT}/`, request);
    },
    async get_allDatatable(filters = null) {
        let request = {
            params: filters
        }
        return await Api.get(`${END_POINT}/`, request);
    },
    store_confirmation(data) {
        return Api.post(`${END_POINT}/store-confirmation`, data);
    },
    downloadPayments (params) {
        let request = {
            params: params,
            responseType: 'blob'
        }
        return Api.get(`${END_POINT}/download`, request);
    }
}