import Api from './Api';

const END_POINT = 'takings';

export default {
    get_all(filters = null) {        
        let request = {
            params: filters
        }
        return Api.get(`${END_POINT}`, request);
    },
    store(taking) {
        return Api.post(`${END_POINT}`, taking);
    },
    calculate(filters = null) {
        let request = {
            params: filters
        }
        return Api.get(`${END_POINT}/calculate`, request);
    },
    delete(taking) {
        return Api.delete(`${END_POINT}/${taking.id}`);
    },
    download (params) {
        let request = {
            params: params,
            responseType: 'blob'
        }
        return Api.get(`${END_POINT}/download`, request);
    }
}