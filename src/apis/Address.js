import Api from './Api';

const END_POINT = 'addresses';

export default {
    get_all(filters = null) {        
        let request = {
            params: filters
        }
        return Api.get(`${END_POINT}`, request);
    },

    store(address) {
        return Api.post(`${END_POINT}`, address);
    },

    update(address) {
        return Api.put(`${END_POINT}/${address.id}`, address);
    },

    delete(address) {
        return Api.delete(`${END_POINT}/${address.id}`, address);
    },
}