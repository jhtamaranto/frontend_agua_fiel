import Api from './Api';

const END_POINT = 'prices';

export default {
    get_all(filters = null) {        
        let request = {
            params: filters
        }
        return Api.get(`${END_POINT}`, request);
    },

    store(price) {
        return Api.post(`${END_POINT}`, price);
    },

    update(price) {
        return Api.put(`${END_POINT}/${price.id}`, price);
    },

    delete(price) {
        return Api.delete(`${END_POINT}/${price.id}`, price);
    },
}