import Api from './Api'

const END_POINT = 'cashflow'

export default {
    getCurrent() {
        return Api.get(`${END_POINT}/current`)
    },

    open(cashData) {
        return Api.post(`${END_POINT}/open`, cashData)
    },

    close(cashData) {
        return Api.patch(`${END_POINT}/close`, cashData)
    },

    saveOperation(operationData) {
        return Api.post(`${END_POINT}/operations`, operationData)
    },

    getPayments(dateFilter, cash_id) {
        let request = {
            params: {
                date_filter: dateFilter,
                cash_id: cash_id
            }
        }
        return Api.get(`${END_POINT}/payments-debts`, request)
    },

    getSales(payment_method, dateFilter, cash_id) {
        let request = {
            params: {
                method: payment_method,
                date_filter: dateFilter,
                cash_id: cash_id
            }
        }
        return Api.get(`${END_POINT}/payments-cash`, request)
    },

    getOperations(type, dateFilter, cash_id) {
        let request = {
            params: {
                type: type,
                date_filter: dateFilter,
                cash_id: cash_id
            }
        }
        return Api.get(`${END_POINT}/operations`, request)
    },

    getTotalAmounst(dateFilter, cash_id) {
        let request = {
            params: {
                date_filter: dateFilter,
                cash_id: cash_id
            }
        }
        return Api.get(`${END_POINT}/total-amounts`, request)
    },

    getMovementsUser (cash_id) {
        let request = {
            params: {
                cash_id: cash_id
            }
        }
        return Api.get(`${END_POINT}/movements-user`, request);
    }
}