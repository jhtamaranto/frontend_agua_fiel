import Api from './Api';

const END_POINT = 'notifications';

export default {

    create() {
        return Api.post(`${END_POINT}/generate`);
    },

    getNotifications() {
        return Api.get(`${END_POINT}/list`);
    },

    getTypesNotifications() {
        return Api.get(`${END_POINT}/get-types`);
    },

    storeTypesNotifications(notify) {
        return Api.post(`${END_POINT}/store-types`, notify);
    },

}