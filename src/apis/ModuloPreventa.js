import Api from './Api';

const END_POINT_ZONA = 'zona';
const END_POINT_ASIGNCION = 'asignacion';

export default {
    zona: {
        findAll(filters = null) {
            let request = {
                params: filters
            }
            return Api.get(`${END_POINT_ZONA}`, request);
        },

        store(zona) {
            return Api.post(`${END_POINT_ZONA}`, zona);
        },

        update(zona) {
            return Api.put(`${END_POINT_ZONA}/${zona.id}`, zona);
        },

        delete(id) {
            return Api.delete(`${END_POINT_ZONA}/${id}`);
        },
        search(search) {
            let request = {
                params: {search}
            }
            return Api.get(`${END_POINT_ZONA}/search`,request);
        },
    },
    asignacion:{
        getPrevendedores(){
                return Api.get(`${END_POINT_ASIGNCION}/prevendedores`);
        },
        getClientes(filters){
            let request={
                params:filters
            }
            return Api.get(`${END_POINT_ASIGNCION}/clientes`,request)
        }
    }
}