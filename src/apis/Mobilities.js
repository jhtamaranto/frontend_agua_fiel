import Api from './Api'

const END_POINT = 'mobilities';

export default {
    canAttendOrder(user_id, order_id) {
        let request = {
            params: {
                user_id,
                order_id
            }
        }
        return Api.get(`${END_POINT}/can-attend`, request);
    }
}