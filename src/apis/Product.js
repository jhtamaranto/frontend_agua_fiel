import Api from './Api'

const END_POINT = 'products'

export default {
    get_all(filters = null) {        
        let request = {
            params: filters
        }
        return Api.get(`${END_POINT}`, request);
    },
    getSummary(product_id = null) {
        let request = {
            params: {
                product_id
            }
        }
        return Api.get(`${END_POINT}/summary`, request)
    },
    get_all_has_return(filters = null) {        
        let request = {
            params: filters
        }
        return Api.get(`${END_POINT}/with-return`, request);
    },
    delete(product) {
        return Api.delete(`${END_POINT}/delete/${product.id}`);
    }
}