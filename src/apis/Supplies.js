import Api from './Api';

const END_POINT = 'supplies';

export default {
    get_all(filters = null) {        
        let request = {
            params: filters
        }
        return Api.get(`${END_POINT}`, request);
    },
    get_brands(filters = null) {        
        let request = {
            params: filters
        }
        return Api.get(`${END_POINT}/brands`, request);
    },
    download (params) {
        let request = {
            params: params,
            responseType: 'blob'
        }
        return Api.get(`${END_POINT}/download`, request);
    },
    store_decrease(data) {
        return Api.post(`${END_POINT}/store-decrease`, data);
    },
}