import Api from './Api'

const END_POINT = 'reports'

export default {
    purchases(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/purchases`, request)
    },
    getPurchaseDetail(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/purchases-detail`, request)
    },
    debts(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/debts`, request)
    },
    debtsDatatable(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/debts/datatable`, request)
    },
    sales(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/sales`, request)
    },
    salesDatatable(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/sales/datatable`, request)
    },
    sumSalesDatatable(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/sales/sumSalesDatatable`, request)
    },
    orders(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/orders`, request)
    },
    ordersDatatable(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/orders/datatable`, request)
    },
    cash(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/cash`, request)
    },
    storeCreditNote (data) {
        return Api.post(`${END_POINT}/sales/canceled`, data);
    },

    getCreditNotes(params){
        let request = {
            params: params
        }
        return Api.get(`${END_POINT}/credit-notes`, request);
    },
    downloadSalesAccounting (params) {
        let request = {
            params: params,
            responseType: 'blob'
        }
        return Api.get(`${END_POINT}/sales-accounting`, request);
    },
    downloadSales (params) {
        let request = {
            params: params,
            responseType: 'blob'
        }
        return Api.get(`${END_POINT}/sales-download`, request);
    }
}