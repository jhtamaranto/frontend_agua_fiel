import Api from './Api'

const END_POINT = 'roles'

export default {

    getAvailablePermits() {
        return Api.get(`${END_POINT}/permissions`)
    }

}