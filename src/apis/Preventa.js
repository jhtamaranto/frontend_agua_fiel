import Api from './Api';

const END_POINT = 'preventa';

export default {
    get_all(filters = null) {
        let request = {
            params: filters
        }
        return Api.get(`${END_POINT}`, request);
    },

    getPriceCustomerProduct(product_id,customer_id){
        let request={
            params:{ product_id,customer_id}
        }
        return Api.get(`/prices/customer/product`, request);
    },
    store(price) {
        return Api.post(`${END_POINT}`, price);
    },

    update(price) {
        return Api.put(`${END_POINT}/${price.id}`, price);
    },

    delete(price) {
        return Api.delete(`${END_POINT}/${price.id}`, price);
    },
}