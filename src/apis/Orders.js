import Api from './Api';

const END_POINT = 'orders';

export default {
    getSummary(customer_id = null) {
        let request = {
            params: {
                customer_id
            }
        }
        return Api.get(`${END_POINT}/summary`, request)
    },
    async get_by_order_id(filters = null) {        
        let request = {
            params: filters
        }
        return await Api.get(`${END_POINT}/get-by-id`, request);
    },

}