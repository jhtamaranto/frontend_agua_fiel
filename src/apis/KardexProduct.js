import Api from './Api'

const END_POINT = 'kardex-product'

export default {
    getKardexByInventory(inventory) {
        let request = {
            params: {
                inventory_id: inventory
            }
        }
        return Api.get(END_POINT, request)
    },
}