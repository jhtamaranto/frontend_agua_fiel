import Api from './Api'

const END_POINT = 'dashboard'

export default {
    getSoldProducts(month, year) {
        let request = {
            params: {
                month: month,
                year: year
            }
        }
        return Api.get(`${END_POINT}/sold-products`, request)
    },
    getSalesMonth(month, year) {
        let request = {
            params: {
                month: month,
                year: year
            }
        }
        return Api.get(`${END_POINT}/sales-month`, request)
    }
}