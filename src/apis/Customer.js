import Api from './Api'

const END_POINT = 'customers'

export default {
    async get_all(filters = null) {        
        let request = {
            params: filters
        }
        return await Api.get(`${END_POINT}`, request)
    },
    async get_all_database(filters = null) {
        let request = {
            params: filters
        }
        //console.log(request)
        return await Api.get(`${END_POINT}/datatable`, request)
    },
    
    getSummary(customer_id = null) {
        let request = {
            params: {
                customer_id
            }
        }
        return Api.get(`${END_POINT}/summary`, request)
    },

    create(customer) {
        return Api.post(`${END_POINT}`, customer);
    },

    async update(customer) {
        return await Api.patch(`${END_POINT}/${customer.id}`, customer)
    },

    delete (customer) {
        return Api.delete(`${END_POINT}/${customer.id}`);
    },
}