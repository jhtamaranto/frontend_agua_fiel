import Api from './Api'

const END_POINT = 'storehouses'

export default {
    getStorehouses(filter = null) {
        return Api.get(END_POINT, filter)
    },

    getInventoryFromStorehouse(storehouse) {
        let request = {
            params: {
                storehouse_id: storehouse
            }
        }
        return Api.get(`${END_POINT}/inventories`, request)
    },

    getInventoryFromStorehouseMain() {
        return Api.get(`${END_POINT}/inventory-main`)
    },

    saveRecharge(data) {
        return Api.post(`${END_POINT}/recharge`, data);
    },

    saveDecrease(data) {
        return Api.post(`${END_POINT}/decrease`, data);
    },

    saveDischarge(data) {
        return Api.post(`${END_POINT}/discharge`, data);
    }
}
