import Api from './Api';

const END_POINT = 'drums';

export default {
    async get_all(filters = null) {        
        let request = {
            params: filters
        }
        return await Api.get(`${END_POINT}/`, request);
    },

    async get_brands_drums(filters = null) {        
        let request = {
            params: filters
        }
        return await Api.get(`${END_POINT}/brands`, request);
    },

    store_loan(data) {
        return Api.post(`${END_POINT}/store-loan`, data);
    },

    store_return(data) {
        return Api.post(`${END_POINT}/store-return`, data);
    },

    delete_drum_movement(drum_id) {
        return Api.delete(`${END_POINT}/delete/${drum_id}`);
    },

    confirm_return(data) {
        return Api.post(`${END_POINT}/confirm-return`, data);
    },
    downloadDetailed (params) {
        let request = {
            params: params,
            responseType: 'blob'
        }
        return Api.get(`${END_POINT}/download-detail`, request);
    },
    downloadSummary (params) {
        let request = {
            params: params,
            responseType: 'blob'
        }
        return Api.get(`${END_POINT}/download-summary`, request);
    }
}