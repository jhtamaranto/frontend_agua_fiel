import Vue from 'vue'
import Router from 'vue-router'
import newCustomer from '@/components/Customer/newCustomer.vue'
import newProduct from '@/components/Product/newProduct.vue'
import Mobility from '@/components/Mobility/Mobility.vue'
import Login from '@/views/Login.vue'
import Roles from '@/views/Roles.vue'
import Supplies from '@/views/Supplies.vue'
import Assets from '@/views/Assets.vue'
import Sales from '@/views/sales/Sales.vue'
import Orders from '@/views/orders/Orders.vue'
import Production from '@/views/production/Production.vue'
import Purchases from '@/views/production/Purchases.vue'
import Providers from '@/views/production/Providers.vue'
import Inventory from '@/views/production/Inventory.vue'
import Debts from '@/views/debts/Debts.vue'
import ProviderAccount from '@/views/providers/ProviderAccount.vue'
import Storehouse from '@/views/storehouses/Storehouse.vue'
import Cashflow from '@/views/cashflow/Cashflow.vue'
import ReportPurchases from '@/views/reports/Purchases.vue'
import ReportDebts from '@/views/reports/Debts.vue'
import ReportSales from '@/views/reports/Sales.vue'
import ReportOrders from '@/views/reports/Orders.vue'
import HistoryCash from '@/views/cashflow/HistoryCash.vue'
import Dashboard from '@/views/dashboard/Dashboard.vue'
import Users from '@/views/users/User.vue'
import Notifications from '@/views/notifications/Notifications.vue'
import ReportCreditNotes from '@/views/reports/CreditNotes.vue'
import LoansReturns from '@/views/reports/LoansReturns'
import DetailPayments from '@/views/reports/DetailPayments';
import Takings from '@/views/cashflow/Takings'
import Preventas from '@/views/preventa/Preventas'
import Venta from '@/views/venta/Ventas'
import Zonas from "@/views/preventas/zona/Zonas";
import Asignacion from "@/views/preventas/asignacion/Asignacion";

Vue.use(Router)

export default new Router({
    routes: [
        {
          path: '/',
          name: 'login',
          component: Login
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
          path: '/customers',
          name: 'customers',
          component: newCustomer
        },
        {
          path: '/products',
          name: 'products',
          component: newProduct
        },
        {
          path: '/mobilities',
          name: 'mobilities',
          component: Mobility
        },
        {
          path: '/roles',
          name: 'roles',
          component: Roles
        },
        {
          path: '/supplies',
          name: 'supplies',
          component: Supplies
        },
        {
          path: '/assets',
          name: 'assets',
          component: Assets
        },
        {
          path: '/sales',
          name: 'sales',
          component: Sales
        },
        {
          path: '/orders',
          name: 'orders',
          component: Orders
        },
        {
          path: '/production',
          name: 'production',
          component: Production
        },
        {
          path: '/debts',
          name: 'debts',
          component: Debts
        },
        {
          path: '/purchases',
          name: 'purchases',
          component: Purchases
        },
        {
          path: '/providers',
          name: 'providers',
          component: Providers
        },
        {
          path: '/inventory',
          name: 'inventory',
          component: Inventory
        },
        {
          path: '/provider-account',
          name: 'provider-account',
          component: ProviderAccount
        },
        {
          path: '/storehouses',
          name: 'storehouses',
          component: Storehouse
        },
        {
          path: '/cashflow',
          name: 'cashflow',
          component: Cashflow
        },
        {
          path: '/report-purchases',
          name: 'report-purchases',
          component: ReportPurchases
        },
        {
          path: '/report-debts',
          name: 'report-debts',
          component: ReportDebts
        },
        {
          path: '/report-sales',
          name: 'report-sales',
          component: ReportSales
        },
        {
          path: '/report-orders',
          name: 'report-orders',
          component: ReportOrders
        },
        {
          path: '/history-cash',
          name: 'history-cash',
          component: HistoryCash
        },
        {
          path: '/report-credit-notes',
          name: 'report-credit-notes',
          component: ReportCreditNotes
        },
        {
          path: '/dashboard',
          name: 'dashboard',
          component: Dashboard
        },
        {
          path: '/users',
          name: 'users',
          component: Users
        },
        {
          path: '/notifications',
          name: 'notifications',
          component: Notifications
        },
        {
          path: '/loans-returns',
          name: 'loans-returns',
          component: LoansReturns
        },
        {
          path: '/detail-payments',
          name: 'detail-payments',
          component: DetailPayments
        },
        {
          path: '/takings',
          name: 'takings',
          component: Takings
        },
        {
            path: '/preventa',
            name: 'preventa',
            component: Preventas
        },
        {
            path: '/venta',
            name: 'venta',
            component: Venta
        },
        {
            path: '/zonas',
            name: 'zonas',
            component: Zonas
        },
        {
            path: '/asignacion',
            name: 'asignacion',
            component: Asignacion
        }

    ]
})