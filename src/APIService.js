import axios from 'axios';
import Api from './apis/Api';

const API_URL = process.env.VUE_APP_API_URL;
//const API_URL = 'http://localhost:3333';
//const API_URL = 'http://api.aguaeberia.com';
//const API_URL = 'http://apidespacho.wost.pe';
export class APIService{

    constructor(){
    }

    /*
    * Login
    */
   login(username, password){
       /*
        const url = `${API_URL}/login`;
        return axios.post(url, {
            username: username,
            password: password
        });
        */

        return Api.post('login', {
            username: username,
            password: password
        });
    }

    async getCustomers() {
        const url = `${API_URL}/customers/`;
        const response = await axios.get(url);
        return response;
    }

    createCustomer(customer){
        const url = `${API_URL}/customers`;
        return axios.post(url,customer);
    }

    updateCustomer(id, customer){
        const url = `${API_URL}/customers/${id}`;
        return axios.patch(url,customer);
    }

    deleteCustomer(id){
        const url = `${API_URL}/customers/${id}`;
        return axios.delete(url);
    }

    changeStatusCustomer(id){
        const url = `${API_URL}/customers/${id}/change-status`;
        return axios.post(url);
    }

    validateDocument(customer){
        const url = `${API_URL}/validate-document`;
        return axios.post(url,customer);
    }

    async getCustomersDropdown() {
        const url = `${API_URL}/customers/dropdown`;
        const response = await axios.get(url);
        return response;
    }

    async getDebstByCustomer(customer){
        const url = `${API_URL}/customers/debts`
        const response = await axios.get(url, customer)
        return response
    }

    /*
    * API Categories
    */
    async getCategoriesDropdown() {
        const url = `${API_URL}/categories/dropdown`;
        const response = await axios.get(url);
        return response;
    }

    /*
    * API Units
    */
    async getUnitsDropdown() {
        const url = `${API_URL}/units/dropdown`;
        const response = await axios.get(url);
        return response;
    }

    /*
    * API Supply
    */
    async getSupplies() {
        const url = `${API_URL}/supplies`;
        const response = await axios.get(url);

        return response;
    }

    async createSupply(supply){
        const url = `${API_URL}/supplies`;
        return axios.post(url,supply);
    }

    async updateSupply(id, supply){
        const url = `${API_URL}/supplies/${id}`;
        return axios.patch(url,supply);
    }

    async inactivateSupply(id){
        const url = `${API_URL}/supplies/${id}`;
        return axios.delete(url);
    }

    /*
    * API Assets
    */
    async getAssets() {
        const url = `${API_URL}/assets`;
        const response = await axios.get(url);

        return response;
    }

    async createAsset(asset){
        const url = `${API_URL}/assets`;
        return axios.post(url,asset);
    }

    async updateAsset(id, asset){
        const url = `${API_URL}/assets/${id}`;
        return axios.patch(url,asset);
    }

    async inactivateAsset(id){
        const url = `${API_URL}/assets/${id}`;
        return axios.delete(url);
    }


    /*
    * API Products
    */
    async getSuppliesDropdown() {
        const url = `${API_URL}/supplies/dropdown`;
        const response = await axios.get(url);

        return response;
    }

    /*
    async getSupplies() {
        const url = `${API_URL}/api/supplies`;
        const response = await axios.get(url);

        return response;
    }
    */

    async getSupplyItem(supply_id) {
        const url =  `${API_URL}/supplies/${supply_id}`;
        const response = await axios.get(url);
        return response;
    }

    async getComposition(product_id) {
        const url =  `${API_URL}/composition/${product_id}/by-product`;
        const response = await axios.get(url);
        return response;
    }

    async getProducts() {
        const url = 'products' ;//`${API_URL}/products/`;
        const response = await Api.get(url);
        return response;
    }

    async createProduct(product){
        const url = `${API_URL}/products`;
        return axios.post(url, product);
    }

    async updateProduct(id, product){
        const url = `${API_URL}/products/${id}`;
        return axios.patch(url,product);
    }

    async inactivateProduct(id){
        const url = `${API_URL}/products/${id}`;
        return axios.delete(url);
    }

    /*
    * API Mobilities
    */
   async getDeliveryUsersDropdown() {
        const url = `${API_URL}/delivery-users/dropdown`;
        const response = await axios.get(url);

        return response;
    }

   async getMobilities() {
        const url = `${API_URL}/mobilities/`;
        const response = await axios.get(url);
        return response;
    }

    async createMobility(mobility){
        const url = `${API_URL}/mobilities`;
        return axios.post(url, mobility);
    }

    async updateMobility(id, mobility){
        const url = `${API_URL}/mobilities/${id}`;
        return axios.patch(url,mobility);
    }

    async inactivateMobility(id){
        const url = `${API_URL}/mobilities/${id}`;
        return axios.delete(url);
    }

    /*
    + API Resources
    */
   async getResources() {
        const url = `${API_URL}/resources/`;
        const response = await axios.get(url);
        return response;
    }

    /*
    * Api de Roles de usuario
    */
    async getRoles() {
        const url = `${API_URL}/roles/`;
        const response = await axios.get(url);
        return response;
    }

    async createRole(role){
        const url = `${API_URL}/roles`;
        return axios.post(url, role);
    }

    async updateRole(id, role){
        const url = `${API_URL}/roles/${id}`;
        return axios.patch(url,role);
    }

    async inactivateRole(id){
        const url = `${API_URL}/roles/${id}`;
        return axios.delete(url);
    }
    
    /*
    * API to point of sale
    */
   async getCatalogue(){
       const url = `${API_URL}/catalogue`
       const response = await axios.get(url)
       return response
   }

    /*
    * API to orders
    */
    async getDeliveryUsers() {
        const url = `${API_URL}/delivery-users`;
        const response = await axios.get(url);

        return response;
    }

    async getStatusOrders(){
        const url = `${API_URL}/orders-status`
        const response = await axios.get(url)
        return response
    }

    async getOrderStatusDropDown(){
        const url = `${API_URL}/orders-status/dropdown`
        const response = await axios.get(url)
        return response
    }

    async getOrders(params = null){
        let request = {
            params: params
        }
        const url = `${API_URL}/orders`
        const response = await axios.get(url, request)
        return response
    }

    async getOrdersByCustomer(customer){
        const url = `${API_URL}/orders/by-customer`
        const response = await axios.get(url, customer)
        return response
    }

    createOrder(order){
        //const url = `${API_URL}/orders`;
        //return axios.post(url, order);
        const url = 'orders';
        return Api.post(url, order);
    }

    async updateOrder(order){
        //const url = `${API_URL}/orders/update`;
        //return axios.post(url, order);
        const url = 'orders/update';
        return Api.post(url, order);
    }

    async changeStatusOrder(order){
        //const url = `${API_URL}/orders/change-status`;
        //return axios.patch(url,order);
        const url = 'orders/change-status';
        return Api.patch(url, order);
    }

    async assingFinalDeliveryMan(order){
        //const url = `${API_URL}/orders/assing-final-deliveryman`;
        //return axios.patch(url,order);
        const url = 'orders/assing-final-deliveryman';
        return Api.patch(url, order);
    }

    /*
    * API PAYMENTS
    */
    async createPayment(payment){
        //const url = `${API_URL}/payments/register`;
        //return axios.post(url, payment);
        const url = 'payments/register';
        return Api.post(url, payment);
    }

    /*
    * API Production
    */
    async getSuppliesProduction() {
        const url = `${API_URL}/production`;
        const response = await axios.get(url);

        return response;
    }

    /*
    * API Debts
    */
    async getPendingDebts(filter = null) {
        const url = `${API_URL}/debts/pending`;
        const response = await axios.get(url, filter);

        return response;
    }

    async getDebtsByCustomer(customer){
        const url = `${API_URL}/debts/by-customer`
        const response = await axios.get(url, customer)
        return response
    }

    async registerPaymentDebt(payment){
        //const url = `${API_URL}/debts/register-payment`;
        //return axios.post(url, payment);
        const url = 'debts/register-payment';
        return Api.post(url, payment);
    }

    async getPaymentsByDebt(debt){
        const url = `${API_URL}/debts/payments`
        const response = await axios.get(url, debt)
        return response
    }

    /*
    * Api for Providers
    */
    async getProviders() {
        const url = `${API_URL}/providers/`;
        const response = await axios.get(url);
        return response;
    }
    
    createProviders(providers){
        const url = `${API_URL}/providers`;
        return axios.post(url,providers);
    }

    async updateProvider(id, provider){
        const url = `${API_URL}/providers/${id}`;
        return axios.patch(url,provider);
    }

    async changeStatusProvider(id){
        const url = `${API_URL}/providers/${id}`;
        return axios.delete(url);
    }

    /*
    * Api for purchases
    */
    async createPurchases(purchase){
        const url = `${API_URL}/purchases`;
        return axios.post(url, purchase);
    }

    /*
    * Api for inventory supplies
    */
    async getKardexBySupply(supply){
        const url = `${API_URL}/kardex-by-supply`
        const response = await axios.get(url, supply)
        return response
    }

    async saveNewInventory(new_stocks){
        const url = `${API_URL}/kardex-save-inventory`;
        return axios.post(url, new_stocks);
    }

    /*
    * API Provider Account
    */
    async getProvidersAccount(filter = null) {
        const url = `${API_URL}/provider-account/pending`;
        const response = await axios.get(url, filter);

        return response;
    }

    async getDebtsByProvider(provider){
        const url = `${API_URL}/provider-account/by-provider`
        const response = await axios.get(url, provider)
        return response
    }

    async amortizeProviderAccount(payment){
        const url = `${API_URL}/provider-account/amortize`;
        return axios.post(url, payment);
    }

}