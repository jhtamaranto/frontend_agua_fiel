import axios from 'axios';

const API_URL = process.env.VUE_SUNAT_RENIEC_URL;

export default {
    async searchDocument (document) {
        //const url = `https://142.93.251.226/apibitel/api/customers/${document}/search-document`;
        const url = `https://apipos.wost.pe/api/customers/${document}/search-document`;
        const response = await axios.get(url);
        return response;
    }
}