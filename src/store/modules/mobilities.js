import ApiMobilities from '../../apis/Mobilities';

export default {
    namespaced: true,
    state: {
        can_attend: null
    },
    mutations: {
        set_cant_attend(state, value){
            state.can_attend = value;
        }
    },
    actions: {
        emptyData({commit}){
            commit('set_cant_attend', null);
        },
        async canAttendOrder({commit}, {user_id, order_id}) {
            return await ApiMobilities.canAttendOrder(user_id, order_id)
                            .then(({data}) => {
                                console.log(data);
                                commit('set_cant_attend', data.can_attend);
                            })
        },
    }
}