import {APIService} from '@/APIService';

const apiService = new APIService();

export default{
    namespaced: true,
    state: {
        order_id: null,
        listProducts: [],
        customer: null,
        discount: 0,
        subtotal: 0,
        amount_igv: 0,
        total: 0,
        total_return: 0,
        productsReturn: [],
        drums: []
    },
    mutations: {
        setOrderId(state, order_id){
            state.order_id = order_id
        },
        setListProducts(state, listProducts) {
            state.listProducts = listProducts
        },
        setDiscount(state, discount) {
            state.discount = discount
        },
        setSubtotal(state, subtotal) {
            state.subtotal = subtotal
        },
        setAmountIgv(state, amount_igv) {
            state.amount_igv = amount_igv
        },
        setTotal(state, total) {
            state.total = total
        },
        setCustomer(state, customer) {
            state.customer = customer
        },
        setTotalReturn(state, total_return) {
            state.total_return = total_return
        },
        set_productsReturn (state, data) {
            state.productsReturn = data;
        },
        add_productsReturn (state, data) {
            state.productsReturn.push(data);
        },
        set_drums (state, data) {
            state.drums = data;
        },
        add_drums (state, data) {
            state.drums.push(data);
        },
        remove_drums (state, index) {
            state.drums.splice(index, 1);
        }
    },
    actions: {
        runSetOrderIdPay({commit}, order_id) {
            commit('setOrderId', order_id)
        },
        runSetSubtotal({commit}, subtotal) {
            commit('setSubtotal', subtotal)
        },
        runSetTotal({commit}, total) {
            commit('setTotal', total)
        },
        runSetAmountIgv({commit}, amount_igv) {
            commit('setAmountIgv', amount_igv)
        },
        runSetListProducts({commit}, listProducts) {
            commit('setListProducts', listProducts)
            commit('set_productsReturn', []);
            commit('set_drums', []);
            let discount = 0
            let total_return = 0
            listProducts.forEach(element => {
                let discount_item = 0;
                if (element.discount_group == true) {
                    discount_item = parseFloat(element.quantity) * element.discount_value;
                } else {
                    discount_item = parseFloat(element.discount_value);
                }
                discount = discount + discount_item;
                if(element.has_return || element.has_return==1 ){
                    if (element.type_sale == 'recarga') {
                        total_return = total_return + element.quantity

                        let productReturn = {
                            value: element.id,
                            text: element.name,
                            type_product: element.type_sale
                        };
                        commit('add_productsReturn', productReturn);
                    }
                }
            })
            
            commit('setDiscount', discount)
            commit('setTotalReturn', total_return)
        },
        runSetCustomer({commit}, customer) {
            commit('setCustomer', null)
            commit('setCustomer', customer)
        },
        getCustomers({commit}){
            return apiService.getCustomersDropdown()
                            .then(({data}) => {
                                commit('setCustomers', data)
                            })
        },
        setDrums({commit}, data) {
            commit('set_drums', data);
        },
        addDrums({commit}, data) {
            commit('add_drums', data);
        },
        removeDrums({commit}, index) {
            commit('remove_drums', index);
        }
    }
}

/*
product = {
    id,
    name,
    quantity,
    base_price,
    discount_amount,
    subtotal
}
*/