import {APIService} from '@/APIService';

import CartTools from '../../helpers/CartTools.js'

const apiService = new APIService();

export default {
    namespaced: true,
    state: {
        catalogue: [],        
        search: '',
        availableProducts: [],
        selectedProducts: [],
        product: null,
        subtotal: null,
        por_igv: null,
        total: null,
        customers: [],
        buyerId: null,
        buyer: null,
        totalQuantity: 0,
        itemSelected: null,
        amount_igv: null,
        editOrder: false,
        orderId: 0,
        total_bonus: 0,
        order_address: null,
        order_reference: null
    },
    mutations: {
        setOrderId(state, orderId) {
            state.orderId = orderId
        },
        setEditOrder(state, editOrder) {
            state.editOrder = editOrder
        },
        setSelectedProducts(state, selectedProducts){
            state.selectedProducts = selectedProducts
        },
        setCatalogue(state, catalogue) {
            state.catalogue = catalogue
        },
        setSearch(state, search) {
            state.search = search
        },
        setShowProduct(state, data) {
            state.catalogue[data.index].show = data.can_show
        },
        setAvailableProducts(state, availableProducts) {
            state.availableProducts = availableProducts
        },
        setProduct(state, product) {
            state.product = product
        },
        setSubtotal(state, subtotal) {
            state.subtotal = subtotal
        },
        setPorIgv(state, por_igv) {
            state.por_igv = por_igv
        },
        setTotal(state, total) {
            state.total = total
        },
        setCustomers(state, customers) {
            state.customers = customers
        },
        setBuyerId(state, buyerId) {
            state.buyerId = buyerId
        },
        setBuyer(state, buyer) {
            state.buyer = buyer
        },
        setTotalQuantity(state, totalQuantity) {
            state.totalQuantity = totalQuantity
        },
        setItemSelected(state, itemSelected) {
            state.itemSelected = itemSelected
        },
        setPriceItemSelected(state, price) {
            state.itemSelected.price = price
        },
        setAmountIgv(state, amount_igv){
            state.amount_igv = amount_igv
        },
        appendAvailableProduct(state, availableProduct) {
            state.availableProducts.push(availableProduct)
        },
        appendSelectedProducts(state, product) {
            state.selectedProducts.push(product)
        },
        replaceProduct(state, data) {
            state.selectedProducts.splice(data.index, 1, data.product)
        },
        removeProduct(state, index) {
            state.selectedProducts.splice(index, 1)
        },
        set_total_bonus (state, value) {
            state.total_bonus = value;
        },
        set_order_address (state, value) {
            state.order_address = value;
        },
        set_order_reference (state, value) {
            state.order_reference = value;
        }
    },
    actions: {
        setOrderAddress ({commit}, value) {
            commit('set_order_address', value);
        },
        setOrderReference ({commit}, value) {
            commit('set_order_reference', value);
        },
        runSetOrderId({commit}, orderId) {
            commit('setOrderId', orderId)
        },
        runSetEditOrder({commit}, editOrder) {
            commit('setEditOrder', editOrder)
        },
        runSetBuyerId({commit}, buyerId) {
            commit('setBuyerId', buyerId)
        },
        runSetPorIgv({commit}, por_igv) {
            commit('setPorIgv', por_igv)
        },
        getCatalogue({commit}){
            return apiService.getCatalogue()
                            .then(({data}) => {
                                commit('setCatalogue', data)
                                commit('setAvailableProducts', data)
                            })
        },
        searchProduct({commit, state}){
            commit('setAvailableProducts', [])
            state.catalogue.forEach( function(element, index) {               
                if (element.name.includes(state.search)) {
                    commit('appendAvailableProduct', element)
                }
            });
        },
        selectProduct({commit, state}) {
            let productSelected = {
                id: state.product.id,
                name: state.product.name,
                unit_price: state.product.unit_price,
                wholesale_price: state.product.wholesale_price,
                preferential_price: state.product.preferential_price,
                base_price: state.product.unit_price,
                price: state.product.price,
                discount_type: null,
                discount_amount: 0,
                quantity: state.product.quantity,
                has_return: state.product.has_return,
                discount_value: 0,
                discount_group: false,
                discount_item: 0,
                type_sale: 'recarga',
                set_discount_customer: false,
                type_product: state.product.type_product,
                is_bonus: false
            }
            let indexFounded = CartTools.findIndexFromItem(state.selectedProducts, state.product.id)  
            if (indexFounded == -1) {
                commit('appendSelectedProducts', productSelected)                
            } else {
                productSelected.type_sale = state.selectedProducts[indexFounded].type_sale;
                //productSelected.quantity = state.selectedProducts[indexFounded].quantity + 1;
                productSelected.base_price = state.selectedProducts[indexFounded].base_price;
                productSelected.discount_type = state.selectedProducts[indexFounded].discount_type;
                productSelected.discount_amount = state.selectedProducts[indexFounded].discount_amount;
                productSelected.discount_value = state.selectedProducts[indexFounded].discount_value;
                productSelected.discount_group = state.selectedProducts[indexFounded].discount_group;
                productSelected.discount_item = state.selectedProducts[indexFounded].discount_item;
                productSelected.price = CartTools.calculatePriceFromProductItem(productSelected)
                let data = {
                    index: indexFounded,
                    product: productSelected
                }
                commit('replaceProduct', data)
            }
            commit('setProduct', null)            
            commit('setTotalQuantity', CartTools.countProductsInCart(state.selectedProducts))
            let cart_amounts = CartTools.calculatCartAmounts(state.selectedProducts);
            commit('setTotal', cart_amounts.subtotal); //commit('setTotal', CartTools.calculateSubTotalPrice(state.selectedProducts))
            commit('set_total_bonus', cart_amounts.total_bonus);
            let subtotal = CartTools.calculateSubTotal(state.total, state.por_igv)
            let amount_igv = state.total - subtotal
            amount_igv = Math.round(amount_igv * 100) / 100
            commit('setSubtotal', subtotal)
            commit('setAmountIgv', amount_igv)
        },
        getCustomers({commit}){
            return apiService.getCustomersDropdown()
                .then(({data}) => {
                    commit('setCustomers', data)
                })
        },
        selectBuyer({commit, state}) {
            let indexFounded = CartTools.findIndexFromItem(state.customers, state.buyerId)           
            if(indexFounded > -1){
                let customer = state.customers[indexFounded];
                commit('setBuyer', state.customers[indexFounded]);
                commit('set_order_address', customer.address);
                commit('set_order_reference', customer.reference);
            }
        },
        settingProduct({commit, state}) {
            let productSelected = {
                id: state.itemSelected.id,
                name: state.itemSelected.name,
                unit_price: state.itemSelected.unit_price,                
                wholesale_price: state.itemSelected.wholesale_price,
                preferential_price: state.itemSelected.preferential_price,
                base_price: state.itemSelected.base_price,
                price: state.itemSelected.base_price,
                discount_type: state.itemSelected.discount_type,
                discount_amount: state.itemSelected.discount_amount,
                quantity: state.itemSelected.quantity,
                has_return: state.itemSelected.has_return,
                discount_value: state.itemSelected.discount_value,
                discount_group: state.itemSelected.discount_group,
                discount_item: state.itemSelected.discount_item,
                type_sale: state.itemSelected.type_sale,
                set_discount_customer: state.itemSelected.set_discount_customer,
                type_product: state.itemSelected.type_product,
                is_bonus: state.itemSelected.is_bonus
            }
            productSelected.price = CartTools.calculatePriceFromProductItem(productSelected)
            let indexFounded = CartTools.findIndexFromItem(state.selectedProducts, productSelected.id)
            let data = {
                index: indexFounded,
                product: productSelected
            }
            commit('replaceProduct', data)
            commit('setItemSelected', null)
            commit('setTotalQuantity', CartTools.countProductsInCart(state.selectedProducts))
            let cart_amounts = CartTools.calculatCartAmounts(state.selectedProducts);
            commit('setTotal', cart_amounts.subtotal); //commit('setTotal', CartTools.calculateSubTotalPrice(state.selectedProducts))
            commit('set_total_bonus', cart_amounts.total_bonus);
            let subtotal = CartTools.calculateSubTotal(state.total, state.por_igv)
            let amount_igv = state.total - subtotal
            amount_igv = Math.round(amount_igv * 100) / 100
            commit('setSubtotal', subtotal)
            commit('setAmountIgv', amount_igv)
        },
        runSetItemSelected({commit}, item) {
            commit('setItemSelected', item)
        },
        registerOrder({commit}, order){
            return apiService.createOrder(order)
                .then(({data}) => {
                    console.log(data)
                })
        },
        clearData({commit, state}) {
            commit('setSelectedProducts', [])
            commit('setTotalQuantity', null)
            commit('setTotal', null)
            commit('setSubtotal', null)
            commit('setAmountIgv', null)
            commit('setBuyerId', null)
            commit('setBuyer', null)
            commit('set_order_address', null);
            commit('set_order_reference', null);
            commit('setOrderId', 0);
            commit('set_total_bonus', 0);
        },
        runAddProductFromOrderEdit({commit, state}, productSelected) {
            commit('appendSelectedProducts', productSelected)            
            commit('setProduct', null)
            commit('setTotalQuantity', CartTools.countProductsInCart(state.selectedProducts))
            let cart_amounts = CartTools.calculatCartAmounts(state.selectedProducts);
            commit('setTotal', cart_amounts.subtotal); //commit('setTotal', CartTools.calculateSubTotalPrice(state.selectedProducts))
            commit('set_total_bonus', cart_amounts.total_bonus);
            let subtotal = CartTools.calculateSubTotal(state.total, state.por_igv)
            let amount_igv = state.total - subtotal
            amount_igv = Math.round(amount_igv * 100) / 100
            commit('setSubtotal', subtotal)
            commit('setAmountIgv', amount_igv)
        },
        deleteProductFromSale({commit, state}) {
            let indexFounded = CartTools.findIndexFromItem(state.selectedProducts, state.product.id)
            if (indexFounded > -1) {
                commit('removeProduct', indexFounded)
            }
            commit('setProduct', null)
            commit('setTotalQuantity', CartTools.countProductsInCart(state.selectedProducts))
            let cart_amounts = CartTools.calculatCartAmounts(state.selectedProducts);
            commit('setTotal', cart_amounts.subtotal); //commit('setTotal', CartTools.calculateSubTotalPrice(state.selectedProducts))
            commit('set_total_bonus', cart_amounts.total_bonus);
            let subtotal = CartTools.calculateSubTotal(state.total, state.por_igv)
            let amount_igv = state.total - subtotal
            amount_igv = Math.round(amount_igv * 100) / 100
            commit('setSubtotal', subtotal)
            commit('setAmountIgv', amount_igv)
        }
    }
}