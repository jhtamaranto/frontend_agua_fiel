import {APIService} from '@/APIService';

const apiService = new APIService();

export default {
    namespaced: true,
    state: {
        supplies: [],
        supplySelected: null,
        kardexSupply: []
    },
    mutations: {
        setKardexSupply(state, kardexSupply) {
            state.kardexSupply = kardexSupply
        },
        setSupplySelected(state, supplySelected) {
            state.supplySelected = supplySelected
        },
        setSupplies(state, supplies){
            state.supplies = supplies
        }
    },
    actions: {
        runsetSupplies({commit}, data) {
            commit('setSupplies', data)
        },
        runSetSupplySelected({commit}, supplySelected) {
            commit('setSupplySelected', null)
            commit('setSupplySelected', supplySelected)
        },
        getSupplies({commit}){
            return apiService.getSuppliesProduction()
                            .then(({data}) => {
                                commit('setSupplies', data)
                            })
        },
        getKardexBySupply({commit, state}) {
            let request = {
                params: {
                    supply_id: state.supplySelected.id
                }
            }
            return apiService.getKardexBySupply(request)
                            .then(({data}) => {
                                commit('setKardexSupply', data)
                            })
        }
    }
}