import {APIService} from '@/APIService';

import OrdersTools from '../../helpers/OrdersTools.js'
import Tools from '../../helpers/Tools.js'

import OrdersApi from '../../apis/Orders';

const apiService = new APIService();

export default {
    namespaced: true,
    state: {
        orders: [],
        ordersFounded: [],
        status: [],
        listStatus: [],
        availableStatus: [],
        order: null,
        deliveryUsers: [],
        deliveryman: null,
        finalDeliveryman: null,
        customers: [],
        history: [],
        debt_customer: null,
        historyFounded: [],
        summary: [],
        disabled_button: false
    },
    mutations: {
        setHistoryFounded(state, historyFounded) {
            state.historyFounded = historyFounded
        },
        setDebtCustomer(state, debt_customer) {
            state.debt_customer = debt_customer
        },
        setOrders(state, orders) {
            state.orders = orders
        },
        setOrdersFounded(state, orders){
            state.ordersFounded = orders
        },
        setOrder(state, order) {
            state.order = order
        },
        setStatus(state, status) {
            state.status = status
        },
        setListStatus(state, listStatus) {
            state.listStatus = listStatus
        },
        setAvailableStatus(state, availableStatus) {
            state.availableStatus = availableStatus
        },
        setDeliveryUsers(state, deliveryUsers) {
            state.deliveryUsers = deliveryUsers
        },
        setDeliveryman(state, deliveryman) {
            state.deliveryman = deliveryman
        },
        setFinalDeliveryman(state, finalDeliveryman) {
            state.finalDeliveryman = finalDeliveryman
        },
        setCustomers(state, customers) {
            state.customers = customers
        },
        setHistory(state, history) {
            state.history = history
        },
        appendOrderFounded(state, order){
            state.ordersFounded.push(order)
        },
        replaceOrders(state, data){
            state.orders.splice(data.index, 1, data.order)
        },
        replaceOrdersFounded(state, data){
            state.ordersFounded.splice(data.index, 1, data.order)
        },
        appendHistoryFounded(state, data){
            state.historyFounded.push(data)
        },
        set_summary (state, data) {
            state.summary = data;
        },
        set_disabled_button (state, value) {
            state.disabled_button = value;
        }
    },
    actions: {
        selectOrder({commit}, order) {
            commit('setOrder', order)
        },
        getListStatusOrder({commit}) {
            return apiService.getStatusOrders()
                            .then( ({data}) => {
                                commit('setListStatus', data)
                            })
        },
        getOrderStatusDropDown({commit}) {
            return apiService.getOrderStatusDropDown()
                            .then(({data}) => {
                                commit('setStatus', data)
                            })
        },
        searchOrders({commit, state}, {type, query}) {
            commit('setOrdersFounded', [])
            state.orders.forEach(function(element, index){
                if(type === 'status') {
                    if(element.status === query) {
                        commit('appendOrderFounded', element)
                    }
                } else if(type === 'customer') {
                    if(element.customer.id === query) {
                        commit('appendOrderFounded', element)
                    }
                } else if (type === 'deliveryman') {
                    if(element.deliveryman == query || element.final_deliveryman == query) {
                        commit('appendOrderFounded', element)
                    }
                }
            })
        },
        getOrders({commit}, params = null) {
            return apiService.getOrders(params)
                            .then(({data}) => {
                                commit('setOrders', data)
                                commit('setOrdersFounded', data)
                            })
        },
        getAvailableStatus({commit, state}) {
            commit('setAvailableStatus', null)
            let available = OrdersTools.getAvailableStatus(state.listStatus, state.order.status)
            commit('setAvailableStatus', available)
        },
        getDeliveryUsers({commit}) {
            commit('setDeliveryUsers', [])
            return apiService.getDeliveryUsers()
                            .then(({data}) => {
                                commit('setDeliveryUsers', data)
                            })
        },
        assignDeliveryman({commit}, deliveryman) {
            commit('setDeliveryman', deliveryman)
        },
        assignFinalDeliveryman({commit}, finalDeliveryman) {
            commit('setFinalDeliveryman', finalDeliveryman)
        },
        replaceOrderUpdated({commit, state}) {
            let index = Tools.findIndexFromItem(state.orders, state.order.id)
            let indexFounded = Tools.findIndexFromItem(state.ordersFounded, state.order.id)
            if (index > -1) {
                let data = {
                    index: index,
                    order: state.order
                }
                commit('replaceOrders', data)
            }
            if (indexFounded > -1) {
                let data = {
                    index: indexFounded,
                    order: state.order
                }
                commit('replaceOrdersFounded', data)
            }
        },
        getCustomers({commit}){
            return apiService.getCustomersDropdown()
                            .then(({data}) => {
                                commit('setCustomers', data)
                            })
        },
        getHistory({commit}, params){
            let request = {
                params: params              
            }
            commit('setHistory', [])
            return apiService.getOrdersByCustomer(request)
                            .then(({data}) => {
                                commit('setHistory', data)
                                commit('setHistoryFounded', data)
                            })
        },
        getDebtCustomer({commit}, customer) {
            let request = {
                params: {
                    customer_id: customer
                }                
            }
            commit('setDebtCustomer', 0)
            return apiService.getDebstByCustomer(request)
                            .then(({data}) => {
                                if(data.total_debt === null){
                                    commit('setDebtCustomer', 0)
                                } else{
                                    commit('setDebtCustomer', data.total_debt)
                                }
                                
                            })
        },
        getOrdersSummary({commit}, params = null) {
            return OrdersApi.getSummary(params)
                .then(({data}) => {
                    commit('set_summary', data)
                })
        },
        setDisabledButton ({commit}, value) {
            commit('set_disabled_button', value);
        }
    }
}