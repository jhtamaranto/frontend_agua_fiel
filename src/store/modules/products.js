import ApiProducts from '../../apis/Product';

export default {
    namespaced: true,
    state: {
        products: [],
        response_products: null
    },
    mutations: {
        set_products (state, data) {
            state.products = data;
        },
        set_response_products (state, value) {
            state.response_products = value;
        }
    },
    actions: {
        async getAllproducts({commit}, filters) {
            return await ApiProducts.get_all(filters)
                .then( response => {
                    commit('set_products', response.data)
                })
                .catch( error => {
                    commit('set_response_products', 'error');
                })
        },
        async getAllHasReturn({commit}, filters) {
            return await ApiProducts.get_all_has_return(filters)
                .then( response => {
                    commit('set_products', response.data)
                })
                .catch( error => {
                    commit('set_response_products', 'error');
                })
        },
        async delete({commit}, product) {
            commit('set_response_products', null);
            return await ApiProducts.delete(product)
                .then( response => {
                    commit('set_response_products', 'success');
                })
                .catch( error => {
                    commit('set_response_products', 'error');
                })
        }
    }
}