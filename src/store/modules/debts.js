import CartTools from '../../helpers/CartTools.js'
import InvoicesApi from '../../apis/Invoices';

import {APIService} from '@/APIService';

const apiService = new APIService();

export default {
    namespaced: true,
    state: {
        debts: [],
        customers: [],
        customerSelected: null,
        debtsCustomer: [],
        selectedDebt: null,
        amountTotal: null,
        amountPaid: null,
        amountPending: null,
        numberCustomers: null,
        type_view: 0,
        invoice: null,
        total_debt: null,
        total_paid: null,
        total_pending: null,
        debt_response: null
    },
    mutations: {
        setTypeView(state, type_view) {
            state.type_view = type_view
        },
        setNumberCustomers(state, numberCustomers) {
            state.numberCustomers = numberCustomers
        },
        setAmountTotal(state, amountTotal){
            state.amountTotal = amountTotal
        },
        setAmountPaid(state, amountPaid) {
            state.amountPaid = amountPaid
        },
        setAmountPending(state, amountPending) {
            state.amountPending = amountPending
        },
        setSelectedDebt(state, selectedDebt) {
            state.selectedDebt = selectedDebt
        },
        setDebtsCustomer(state, debtsCustomer){
            state.debtsCustomer = debtsCustomer
        },
        setDebts(state, debts){
            state.debts = debts
        },
        setCustomers(state, customers) {
            state.customers = customers
        },
        setCustomerSelected(state, customerSelected) {
            state.customerSelected = customerSelected
        },
        replaceItemDebts(state, data) {
            state.debts.splice(data.index, 1, data.debt)
        },
        set_invoice (state, data) {
            state.invoice = data;
        },
        set_total_debt (state, value) {
            state.total_debt = value;
        },
        set_total_paid (state, value) {
            state.total_paid = value;
        },
        set_total_pending (state, value) {
            state.total_pending = value;
        },
        set_debt_response (state, value) {
            state.debt_response = value;
        }
    },
    actions: {
        setInvoice ({commit}, data) {
            commit('set_invoice', data);
        },
        runSetTypeView({commit}, type_view) {
            commit('setTypeView', type_view)
        },
        runSetSelectedDebt({commit}, selectedDebt){
            commit('setSelectedDebt', selectedDebt)
        },
        runSetCustomerSelected({commit}, customerSelected) {
            commit('setCustomerSelected', customerSelected)
        },
        getDebts({commit}, filter = null){
            return apiService.getPendingDebts(filter)
                            .then(({data}) => {
                                commit('setDebts', data)
                                commit('setCustomers', data)
                            })
        },
        getDebtsByCustomer({commit, state}){
            let request = {
                params: {
                    'customer_id': state.customerSelected.id
                }
            }
            commit('setDebtsCustomer', [])
            return apiService.getDebtsByCustomer(request)
                            .then(({data}) => {
                                commit('setDebtsCustomer', data)
                            })
        },
        replaceCustomerDebtInDebtsList({commit, state}, newDebt) {
            let indexFounded = CartTools.findIndexFromItem(state.debts, newDebt.id)
            if (indexFounded >= 0) {
                let data = {
                    index: indexFounded,
                    debt: newDebt
                }
                commit('replaceItemDebts', data)
            }
        },
        getTotalAmount({commit, state}){
            let amountTotal = 0
            let amountPaid = 0
            let amountPending = 0
            let numberCustomers = 0
            state.debts.forEach((element) => {
                amountTotal = amountTotal + parseFloat(element.total_debt)
                amountPaid = amountPaid + parseFloat(element.paid_debt)
                numberCustomers++
            })
            amountPending = amountTotal - amountPaid
            commit('setAmountTotal', amountTotal)
            commit('setAmountPaid', amountPaid)
            commit('setAmountPending', amountPending)
            commit('setNumberCustomers', numberCustomers)
        },
        async generateInvoice({commit}, invoice) {
            commit('set_invoice', null);
            commit('set_debt_response', null);
            return await InvoicesApi.create(invoice)
                .then(({data}) => {
                    commit('set_invoice', data);
                }).catch( error => {
                    commit('set_debt_response', 'error');
                })
        },
        setTotalDebt ({commit}, value) {
            commit('set_total_debt', value);
        },
        setTotalPaid ({commit}, value) {
            commit('set_total_paid', value);
        },
        setTotalPending ({commit}, value) {
            commit('set_total_pending', value);
        }
    }
}