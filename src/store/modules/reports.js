import ApiReport from '../../apis/Reports';

export default {
    namespaced: true,
    state: {
        sales: [],
        credit_note: null,
        error: null
    },
    mutations: {
        set_sales (state, data) {
            state.sales = data;
        },
        set_credit_note (state, data) {
            state.credit_note = data;
        },
        set_error (state, data) {
            state.error = data;
        }
    },
    actions: {
        async storeCreditNote({commit}, creditNote) {
            commit('set_credit_note', null);
            return await ApiReport.storeCreditNote(creditNote)
                .then( response => {
                    commit('set_credit_note', response.data);
                })
                .catch( error => {
                    commit('set_error', 'Hubo un error al anular factura');
                })
        }
    }
}