import ApiNotifications from '../../apis/Notification';

export default {
    namespaced: true,
    state: {
        notifications: [],
        status: null,
        types_notifications: null
    },
    mutations: {
        set_notifications (state, data) {
            state.notifications = data;
        },
        set_status (state, data) {
            state.status = data;
        },
        set_types_notifications (state, data) {
            state.types_notifications = data;
        }
    },
    actions: {
        async createNotification({commit, dispatch}) {
            return await ApiNotifications.create()
                .then( ({data}) => {
                    commit('set_status', data);
                    dispatch('getNotifications');
                })
        },
        async getNotifications({commit}){
            commit('set_notifications', []);
            return await ApiNotifications.getNotifications()
                .then(({data}) => {
                    commit('set_notifications', data);
                })
        },
        async getTypesNotifications({commit}){
            return await ApiNotifications.getTypesNotifications()
                .then(({data}) => {
                    commit('set_types_notifications', data);
                })
        },
        async storeTypesNotifications({commit}, notify) {
            return await ApiNotifications.storeTypesNotifications(notify)
                .then( ({data}) => {
                    commit('set_types_notifications', data);
                })
        },
    }
}