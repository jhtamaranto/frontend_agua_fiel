import {APIService} from '@/APIService';

const apiService = new APIService();

export default {
    namespaced: true,
    state: {
        assetId: null,
        assets: [],
        asset: null
    },
    mutations: {
        setAssetId(state, assetId) {
            state.assetId = assetId
        },
        setAsset(state, asset) {
            state.asset = asset
        },
        setAssets(state, assets) {
            state.assets = assets
        },
        appendAsset(state, asset) {
            state.assets.push(asset)
        },
        replaceAsset(state, asset) {
            let indexFounded = -1
            state.assets.findIndex(function(element, index){
                if(element.id === asset.id){
                    indexFounded = index
                }
            })
            if (indexFounded > -1) {
                state.assets.splice(indexFounded, 1, asset);
            }
        }
    },
    actions: {
        getAssets({commit}) {
            return apiService.getAssets()
                            .then(({data}) => {
                                commit('setAssets', data)
                            })
        },
        createAsset({commit, state}) {
            return apiService.createAsset(state.asset)
                            .then( ({data}) => {
                                commit('appendAsset', data)
                                commit('setAsset', null)
                            })
        },
        updateAsset({commit, state}) {
            return apiService.updateAsset(state.assetId, state.asset)
                            .then( ({data}) => {                                
                                commit('replaceAsset', data)
                                commit('setAsset', null)
                                commit('setAssetId', null)
                            })
        },
        inactivateAsset({commit, state}) {
            return apiService.inactivateAsset(state.assetId)
                            .then( ({data}) => {                                
                                commit('replaceAsset', data)
                                commit('setAsset', null)
                                commit('setAssetId', null)
                            })
        }
    }

}