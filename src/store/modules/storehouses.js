import ApiStorehouses from '../../apis/Storehouses'

import ApiKardexProduct from '../../apis/KardexProduct'

export default {
    namespaced: true,
    state: {
        storehouses: [],
        storehouseSelected: null,
        products: [],
        productsMain: [],
        productSelected: null,
        kardex: []
    },
    mutations: {
        setKardex(state, kardex) {
            state.kardex = kardex
        },
        setStorehouses(state, storehouses) {
            state.storehouses = storehouses
        },
        setStorehouseSelected(state, storehouseSelected) {
            state.storehouseSelected = storehouseSelected
        },
        setProducts(state, products) {
            state.products = products
        },
        setProductsMain(state, products) {
            state.productsMain = products
        },
        setProductSelected(state, product) {
            state.productSelected = product
        }
    },
    actions: {
        runSetProductSelected({commit}, product) {
            commit('setProductSelected', product)
        },
        runSetStorehouses({commit}, storehouses) {
            commit('setStorehouses', [])
            commit('setStorehouses', storehouses)
        },
        runSetStorehouseSelected({commit}, storehouseSelected) {
            commit('setStorehouseSelected', storehouseSelected)
        },
        async getStorehouses({commit}, filter = null) {
            return await ApiStorehouses.getStorehouses(filter)
                .then(({data}) => {
                    commit('setStorehouses', data)
                })
        },
        selectStorehouseMain({commit, state}) {
            let storehouseMain = null
            state.storehouses.forEach(element => {
                if(element.main) {
                    storehouseMain = element
                }
            });
            commit('setStorehouseSelected', storehouseMain)
        },
        async getProductsInStorehouse({commit}, storehouse_id) {
            await ApiStorehouses.getInventoryFromStorehouse(storehouse_id)
                .then(({data}) => {
                    commit('setProducts', data)
                })
        },
        async getProductsInStorehouseMain({commit}) {
            return await ApiStorehouses.getInventoryFromStorehouseMain()
                .then(({data}) => {
                    commit('setProductsMain', data)
                })
        },
        getKardexByInventory({commit}, inventory_id) {
            commit('setKardex', [])
            ApiKardexProduct.getKardexByInventory(inventory_id)
                .then(({data}) => {
                    commit('setKardex', data)
                })
        },
    }
}