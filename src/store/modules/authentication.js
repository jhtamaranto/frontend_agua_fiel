import router from '@/router/index'

import {APIService} from '@/APIService';

const apiService = new APIService();

export default {
    namespaced: true,
    state: {
        token: null,
        loginUsername: null,
        loginPassword: null,
        loginError: null,
        currentUser: null,
        roles: null,
        permissions: null,
        company_name: null,
        company_document: null,
        company_address: null,
        igv: null,
        action_root: null,
        companyData: null,
        class_login: 'fondo-login'
    },
    actions: {
        login({commit, state, dispatch}) {
            commit('setLoginError', null)
            return apiService.login(state.loginUsername, state.loginPassword)
                    .then(({ data }) => {
                        this.$token = data.token.token;
                        localStorage.setItem('access_token', 'Bearer ' + data.token.token);
                        commit('set_class_login', 'fondo-loged');
                        commit('setToken', data.token.token)
                        commit('setCurrentUser', data.user)
                        commit('setRoles', data.roles)
                        commit('setPermissions', data.permissions)
                        commit('setCompanyName', data.company_name)
                        commit('setCompanyDocument', data.company_document)
                        commit('setCompanyAddress', data.company_address)
                        commit('setIgv', data.igv);
                        commit('set_companyData', data.company_data);
                        dispatch('notifications/createNotification', {}, {root: true});
                        if (state.roles[0] == 'REPARTIDOR') {
                            commit('set_action_root', '/orders')
                            router.push('/orders')
                        } else {
                            commit('set_action_root', '/cashflow')
                            router.push('/cashflow')

                        }
                    })
                    .catch(() => {
                        commit('set_class_login', 'fondo-login');
                        commit('setLoginError', 'Usuario no registrado.')
                    })

        },
        logout({commit}) {
            this.$token = '';
            localStorage.removeItem('access_token');
            commit('set_class_login', 'fondo-login');
            commit('setCurrentUser', null)
            commit('setToken', null)
            commit('setRoles', null)
            commit('setPermissions', null)
            commit('set_action_root', null)
            commit('set_companyData', null);
            router.push('/login')
        }
    },
    getters: {
        isLoggedIn(state) {
            return !!state.token
        },
        hasPermission: (state) => (permission) => {
            if (state.roles[0] == 'SUPERADMIN') {
                return true
            } else {
                return state.permissions.includes(permission)
            }
        },
        getIdentifiedUser(state) {
            return state.currentUser
        },
        getUserRoles(state) {
            return state.roles
        }
    },
    mutations: {
        setToken(state, token){
            state.token = token
        },
        setLoginUsername(state, username){
            state.loginUsername = username
        },
        setLoginPassword(state, password){
            state.loginPassword = password
        },
        setLoginError(state, error){
            state.loginError = error
        },
        setCurrentUser(state, currentUser){
            state.currentUser = currentUser
        },
        setRoles(state, roles) {
            state.roles = roles
        },
        setPermissions(state, permissions) {
            state.permissions = permissions
        },
        setCompanyName(state, company_name) {
            state.company_name = company_name
        },
        setCompanyDocument(state, company_document){
            state.company_document = company_document
        },
        setCompanyAddress(state, company_address){
            state.company_address = company_address
        },
        setIgv(state, igv){
            state.igv = igv
        },
        set_action_root(state, value){
            state.action_root = value;
        },
        set_companyData (state, data) {
            state.companyData = data;
        },
        set_class_login (state, value) {
            state.class_login = value;
        }
    }
}