import CartTools from '../../helpers/CartTools.js'

import {APIService} from '@/APIService';

const apiService = new APIService();

export default{
    namespaced: true,
    state: {
        providers: [],
        provider: null,
        debts: [],
        providerSelected: null,
        debtsProvider: [],
        amountTotal: null,
        amountPaid: null,
        amountPending: null,
        numberProviders: null,
        selectedDebt: null,
        type_view: 0
    },
    mutations: {
        setTypeView(state, type_view) {
            state.type_view = type_view
        },
        setNumberProviders(state, numberProviders) {
            state.numberProviders = numberProviders
        },
        setAmountTotal(state, amountTotal){
            state.amountTotal = amountTotal
        },
        setAmountPaid(state, amountPaid) {
            state.amountPaid = amountPaid
        },
        setAmountPending(state, amountPending) {
            state.amountPending = amountPending
        },
        setProviders(state, providers){
            state.providers = providers
        },
        setProvider(state, provider){
            state.provider = provider
        },
        setDebts(state, debts){
            state.debts = debts
        },
        setProviderSelected(state, provider) {
            state.providerSelected = provider
        },
        setDebtsProvider(state, debts) {
            state.debtsProvider = debts
        },
        setSelectedDebt(state, selectedDebt) {
            state.selectedDebt = selectedDebt
        },
        replaceItemDebts(state, data) {
            state.debts.splice(data.index, 1, data.debt)
        }
    },
    actions: {
        runSetTypeView({commit}, type_view) {
            commit('setTypeView', type_view)
        },
        runSetSelectedDebt({commit}, selectedDebt){
            commit('setSelectedDebt', selectedDebt)
        },
        runSetProviderSelected({commit}, provider){
            commit('setProviderSelected', provider)
        },
        runSetProvider({commit}, provider) {
            commit('setProvider', provider)
        },
        getProviders({commit}) {
            commit('setProviders', [])
            return apiService.getProviders()
                .then(({data}) => {
                    commit('setProviders', data)
                })
        },
        getDebts({commit}, filter = null){
            return apiService.getProvidersAccount(filter)
                .then(({data}) => {
                    commit('setDebts', data)
                })
        },
        getDebtsByProvider({commit, state}){
            let request = {
                params: {
                    'provider_id': state.providerSelected.id
                }
            }
            commit('setDebtsProvider', [])
            return apiService.getDebtsByProvider(request)
                            .then(({data}) => {
                                commit('setDebtsProvider', data)
                            })
        },
        getTotalAmount({commit, state}){
            let amountTotal = 0
            let amountPaid = 0
            let amountPending = 0
            let numberProviders = 0
            state.debts.forEach((element) => {
                console.log(element)
                amountTotal = amountTotal + parseFloat(element.total_debt)
                amountPaid = amountPaid + parseFloat(element.paid_debt)
                numberProviders++
            })
            amountPending = amountTotal - amountPaid
            commit('setAmountTotal', amountTotal)
            commit('setAmountPaid', amountPaid)
            commit('setAmountPending', amountPending)
            commit('setNumberProviders', numberProviders)
        },
        replaceProviderDebtInDebtsList({commit, state}, newDebt) {
            let indexFounded = CartTools.findIndexFromItem(state.debts, newDebt.id)
            if (indexFounded >= 0) {
                let data = {
                    index: indexFounded,
                    debt: newDebt
                }
                commit('replaceItemDebts', data)
            }
        }

    }
}