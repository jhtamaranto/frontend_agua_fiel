import {APIService} from '@/APIService';

import ApiRoles from '../../apis/Roles'

const apiService = new APIService();

export default {
    namespaced: true,
    state: {
        roleId: null,
        roles: [],
        role: null,
        resources: [],
        newRole: null,
        permissions: [],
        permission_selected: []
    },
    mutations: {
        setPermissionSelected(state, permissions) {
            state.permission_selected = permissions
        },
        setPermissions(state, permissions) {
            state.permissions = permissions
        },
        setRoleId(state, roleId) {
            state.roleId = roleId
        },
        setRole(state, role) {
            state.role = role
        },
        setRoles(state, roles) {
            state.roles = roles
        },
        appendRole(state, role) {
            state.roles.push(role)
        },
        replaceRoles(state, role) {
            let indexFounded = -1
            state.roles.findIndex(function(element, index){
                if(element.id === role.id){
                    indexFounded = index
                }
            })
            if (indexFounded > -1) {
                state.roles.splice(indexFounded, 1, role);
            }
        },
        setResources(state, resources) {
            state.resources = resources
        },
        setNewRole(state, newRole) {
            state.newRole = newRole
        }
    },
    actions: {
        runSetNewRole({commit}, newRole) {
            commit('setNewRole', newRole)
        },
        getRoles({commit}) {
            return apiService.getRoles()
                            .then(({data}) => {
                                commit('setRoles', data)
                            })
        },
        createRole({commit, state}) {
            return apiService.createRole(state.newRole)
                            .then( ({data}) => {
                                commit('appendRole', data)
                                commit('setNewRole', null)
                            })
        },
        updateRole({commit, state}) {
            return apiService.updateRole(state.roleId, state.role)
                            .then( ({data}) => {                                
                                commit('replaceRoles', data)
                                commit('setRole', null)
                                commit('setRoleId', null)
                            })
        },
        inactivateRole({commit, state}) {
            return apiService.inactivateRole(state.roleId)
                            .then( ({data}) => {                                
                                commit('replaceRoles', data)
                                commit('setRole', null)
                                commit('setRoleId', null)
                            })
        },
        getResources({commit}) {
            return apiService.getResources()
                            .then(({data}) => {
                                commit('setResources', data)
                            })
        },
        getAvailablePermits({commit}) {
            return ApiRoles.getAvailablePermits()
                            .then(({data}) => {
                                commit('setPermissions', data)
                            })
        }
    }

}