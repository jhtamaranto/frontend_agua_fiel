import ApiDrums from '../../apis/Drums';

export default {
    namespaced: true,
    state: {
        drums: [],
        brands_drums: [],
        response_drums: null,
        filters_drums: null,
        total_loan: 0,
        total_return: 0,
    },
    mutations: {
        set_drums (state, data) {
            state.drums = data;
        },
        set_brands_drums (state, data) {
            state.brands_drums = data;
        },
        set_response_drums (state, value) {
            state.response_drums = value;
        },
        set_filters_drums (state, data) {
            state.filters_drums = data;
        },
        set_total_loan (state, value) {
            state.total_loan = value;
        },
        set_total_return (state, value) {
            state.total_return = value;
        }
    },
    actions: {
        setFiltersDrums ({commit}, data) {
            commit('set_filters_drums', data);
        },
        calculateTotals ({commit, state}) {
            let amount_loan = 0;
            let amount_return = 0;
            for (let item of state.drums) {
                if (item.drum_status == 'prestado') {
                    amount_loan = amount_loan + parseInt(item.quantity);
                } else if (item.drum_status == 'devuelto') {
                    amount_return = amount_return + parseInt(item.quantity);
                }
            }
            commit('set_total_loan', amount_loan);
            commit('set_total_return', amount_return);
        },
        async getAllDrums({commit}, filters) {
            return await ApiDrums.get_all(filters)
                .then( response => {
                    commit('set_drums', response.data)
                })
                .catch( error => {
                    commit('set_response_drums', 'error');
                })
        },
        async getAllBrandsDrums({commit}, filters) {
            return await ApiDrums.get_brands_drums(filters)
                .then( response => {
                    commit('set_brands_drums', response.data)
                })
                .catch( error => {
                    commit('set_response_drums', 'error');
                })
        },
        async storeLoan ({commit}, data) {
            commit('set_response_drums', null);
            return await ApiDrums.store_loan(data)
                .then( response => {
                    commit('set_response_drums', 'success');
                }).catch( error => {
                    commit('set_response_drums', 'error');
                })
        },
        async storeReturn ({commit}, data) {
            commit('set_response_drums', null);
            return await ApiDrums.store_return(data)
                .then( response => {
                    commit('set_response_drums', 'success');
                }).catch( error => {
                    commit('set_response_drums', 'error');
                })
        },
        async deleteDrumMovement ({commit}, drum_id) {
            commit('set_response_drums', null);
            return await ApiDrums.delete_drum_movement(drum_id)
                .then( response => {
                    commit('set_response_drums', 'success');
                }).catch( error => {
                    commit('set_response_drums', 'error');
                })
        },
        async confirmReturn ({commit}, data) {
            commit('set_response_drums', null);
            return await ApiDrums.confirm_return(data)
                .then( response => {
                    commit('set_response_drums', 'success');
                }).catch( error => {
                    commit('set_response_drums', 'error');
                })
        },
    }
}