import ApiCustomers from '../../apis/Customer';

export default {
    namespaced: true,
    state: {
        customers: [],
        customerCreated: null,
        response_customers: null
    },
    mutations: {
        set_response_customers (state, value) {
            state.response_customers = value;
        },
        set_customers (state, data) {
            state.customers = data;
        },
        set_customerCreated (state, data) {
            state.customerCreated = data;
        }
    },
    actions: {
        async getAllCustomers({commit}, filters) {
            return await ApiCustomers.get_all(filters)
                .then( response => {
                    commit('set_customers', response.data)
                })
                .catch( error => {
                    commit('set_response_customers', 'error');
                })
        },
        async createCustomer({commit}, customer) {
            commit('set_customerCreated', null);
            commit('set_response_customers', null);
            return await ApiCustomers.create(customer)
                .then( ({data}) => {
                    commit('set_customerCreated', data);
                }).catch( error => {
                    commit('set_response_customers', 'error');
                })
        },
        async update({commit}, customer) {
            commit('set_response_customers', null);
            return await ApiCustomers.update(customer)
                .then( response => {
                    commit('set_response_customers', 'success');
                })
                .catch( error => {
                    commit('set_response_customers', 'error');
                })
        },
    }
}