import ApiDetailPayment from '../../apis/DetailPayment';

export default {
    namespaced: true,
    state: {
        payments: [],
        response_payments: null,
        invoice: null
    },
    mutations: {
        set_payments (state, data) {
            state.payments = data;
        },
        set_response_payments (state, value) {
            state.response_payments = value;
        },
        set_invoice (state, data) {
            state.invoice = data;
        }
    },
    actions: {
        async getAllPayments ({commit}, filters) {
            commit('set_response_payments', null);
            return await ApiDetailPayment.get_all(filters)
                .then( response => {
                    commit('set_payments', response.data);
                }).catch( error => {
                    commit('set_response_payments', 'error');
                })
        },
        async getAllPaymentsDatatable ({commit}, filters) {
            commit('set_response_payments', null);
            return await ApiDetailPayment.get_all(filters)
                .then( response => {
                    commit('set_payments', response.data);
                }).catch( error => {
                    commit('set_response_payments', 'error');
                })
        },
        async storeConfirmation ({commit}, payment) {
            commit('set_response_payments', null);
            return await ApiDetailPayment.store_confirmation(payment)
                .then( response => {
                    commit('set_response_payments', 'success');
                    commit('set_invoice', response.data);
                }).catch( error => {
                    commit('set_response_payments', 'error');
                })
        }
    }
}