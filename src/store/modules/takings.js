import ApiTakings from '../../apis/Takings';

export default {
    namespaced: true,
    state: {
        takings: [],
        taking_response: null,
        taking_calculated: null
    },
    mutations: {
        set_taking (state, data) {
            state.takings = data;
        },
        set_taking_response (state, value) {
            state.taking_response = value;
        },
        set_taking_calculated (state, data) {
            state.taking_calculated = data;
        }
    },
    actions: {
        async getAllTakings({commit}, filters) {
            return await ApiTakings.get_all(filters)
                .then( response => {
                    commit('set_taking', response.data)
                })
                .catch( error => {
                    commit('set_taking_response', 'error');
                })
        },
        async store({commit}, taking) {
            commit('set_taking_response', null);
            return await ApiTakings.store(taking)
                .then( response => {
                    commit('set_taking_response', 'success');
                }).catch( error => {
                    commit('set_taking_response', 'error');
                })
        },
        async calculateTaking({commit}, taking) {
            commit('set_taking_calculated', null);
            return await ApiTakings.calculate(taking)
                .then( response => {
                    commit('set_taking_calculated', response.data);
                    commit('set_taking_response', 'success');
                }).catch( error => {
                    commit('set_taking_response', 'error');
                })
        },
        async delete({commit}, taking) {
            commit('set_taking_response', null);
            return await ApiTakings.delete(taking)
                .then( response => {
                    commit('set_taking_response', 'success');
                }).catch( error => {
                    commit('set_taking_response', 'error');
                })
        },
    }
}