import ApiCashflow from '../../apis/Cashflow'
import ApiReports from '../../apis/Reports';

import Tools from '../../helpers/Tools'

export default {
    namespaced: true,
    state: {
        dateFilter: null,
        cashCurrent: null,
        cashData: [],
        incomes: [],
        expenses: [],
        payments: [],
        salesCash: [],
        salesCards: [],
        balanceFavors: [],
        totalIncomes: 0,
        totalExpenses: 0,
        totalPayments: 0,
        totalSalesCash: 0,
        totalSalesCards: 0,
        totalSales: 0,
        totalCash: 0,
        totalMoney: 0,
        totalCards: 0,
        cashSelected: null,
        cashItem: null,
        history: [],
        movementsUser: []
    },
    mutations: {
        setDateFilter(state, dateFilter) {
            state.dateFilter = dateFilter
        },
        setBalanceFavors(state, balanceFavors) {
            state.balanceFavors = balanceFavors
        },
        setTotalMoney(state, totalMoney) {
            state.totalMoney = totalMoney
        },
        setTotalCards(state, totalCards) {
            state.totalCards = totalCards
        },
        setTotalCash(state, totalCash) {
            state.totalCash = totalCash
        },
        setCashCurrent(state, cashCurrent) {
            state.cashCurrent = cashCurrent
        },
        setCashData(state, cashData) {
            state.cashData = cashData
        },
        setIncomes(state, incomes) {
            state.incomes = incomes
        },
        setExpenses(state, expenses) {
            state.expenses = expenses
        },
        setPayments(state, payments) {
            state.payments = payments
        },
        setSalesCash(state, salesCash) {
            state.salesCash = salesCash
        },
        setSalesCards(state, salesCards) {
            state.salesCards = salesCards
        },
        setTotalIncomes(state, totalIncomes) {
            state.totalIncomes = totalIncomes
        },
        setTotalExpenses(state, totalExpenses) {
            state.totalExpenses = totalExpenses
        },
        setTotalPayments(state, totalPayments) {
            state.totalPayments = totalPayments
        },
        setTotalSalesCash(state, totalSalesCash) {
            state.totalSalesCash = totalSalesCash
        },
        setTotalSalesCards(state, totalSalesCards) {
            state.totalSalesCards = totalSalesCards
        },
        setTotalSales(state, totalSales) {
            state.totalSales = totalSales
        },
        set_cashSelected (state, data) {
            state.cashSelected = data;
        },
        set_cashItem (state, data) {
            state.cashItem = data;
        },
        set_history (state, data) {
            state.history = data;
        },
        set_movementsUser (state, data) {
            state.movementsUser = data;
        }
    },
    actions: {
        runSetDateFilter({commit}, dateFilter) {
            commit('setDateFilter', dateFilter)
        },
        openCash({commit}, params) {
            ApiCashflow.open(params)
                    .then(({data}) => {
                        commit('setCashCurrent', data)
                    })
        },
        async getCashCurrent({commit}) {
            commit('setCashCurrent', null);
            return await ApiCashflow.getCurrent()
                    .then(({data}) => {
                        commit('setCashCurrent', data)
                    })
        },
        getPayments({commit, state}, cash_id) {
            if(!state.dateFilter) {
                commit('setDateFilter', Tools.prepareCurrentDate())
            }
            ApiCashflow.getPayments(state.dateFilter, cash_id)
                    .then(({data}) => {
                        commit('setCashData', data)
                        commit('setPayments', data)
                    })
        },
        getSalesCash({commit, state}, cash_id) {
            if(!state.dateFilter) {
                commit('setDateFilter', Tools.prepareCurrentDate())
            }
            ApiCashflow.getSales('CASH', state.dateFilter, cash_id)
                    .then(({data}) => {
                        commit('setCashData', data)
                        commit('setSalesCash', data)
                    })
        },
        getSalesCards({commit, state}, cash_id) {
            if(!state.dateFilter) {
                commit('setDateFilter', Tools.prepareCurrentDate())
            }
            ApiCashflow.getSales('TARJETAS', state.dateFilter, cash_id)
                    .then(({data}) => {
                        commit('setCashData', data)
                        commit('setSalesCards', data)
                    })
        },
        async getIncomes({commit, state}, cash_id) {
            if(!state.dateFilter) {
                commit('setDateFilter', Tools.prepareCurrentDate())
            }
            return await ApiCashflow.getOperations('income', state.dateFilter, cash_id)
                    .then(({data}) => {
                        commit('setCashData', data)
                        commit('setIncomes', data)
                    })
        },
        getExpenses({commit, state}, cash_id) {
            if(!state.dateFilter) {
                commit('setDateFilter', Tools.prepareCurrentDate())
            }
            ApiCashflow.getOperations('expense', state.dateFilter, cash_id)
                    .then(({data}) => {
                        commit('setCashData', data)
                        commit('setExpenses', data)
                    })
        },
        getBalanceFavor({commit, state}, cash_id) {
            if(!state.dateFilter) {
                commit('setDateFilter', Tools.prepareCurrentDate())
            }
            ApiCashflow.getOperations('balance_favor', state.dateFilter, cash_id)
                    .then(({data}) => {
                        commit('setCashData', data)
                        commit('setBalanceFavors', data)
                    })
        },
        getTotalAmount({commit, state}, cash_id) {
            if(!state.dateFilter) {
                commit('setDateFilter', Tools.prepareCurrentDate())
            }
            ApiCashflow.getTotalAmounst(state.dateFilter, cash_id)
                    .then( ({data}) => {
                        commit('setTotalCash', data.totalCash)
                        commit('setTotalIncomes', data.totalIncomes)
                        commit('setTotalExpenses', data.totalExpenses)
                        commit('setTotalPayments', data.totalPayments)
                        commit('setTotalSalesCash', data.totalSalesCash)
                        commit('setTotalSalesCards', data.totalSalesCards)
                        commit('setTotalMoney', data.totalMoney)
                        commit('setTotalCards', data.totalCards)
                    } )
        },
        selectCash({commit}, data) {
            commit('set_cashSelected', data);
        },
        async getTotalByCash({commit}, {cash_id, date_filter}) {
            commit('set_cashItem', null);
            await ApiCashflow.getTotalAmounst(date_filter, cash_id)
                .then( ({data}) => {
                    commit('set_cashItem', data);
                } )
        },
        async getHistory({commit}, params) {
            await ApiReports.cash(params)
                .then( ({data}) => {
                    commit('set_history', data);
                } )
        },
        async getMovementsUser({commit}, cash_id) {
            commit('set_movementsUser', []);
            await ApiCashflow.getMovementsUser(cash_id)
                .then(({data}) => {
                    commit('set_movementsUser', data)
                })
        },
    }
}