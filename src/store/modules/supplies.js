import {APIService} from '@/APIService';

const apiService = new APIService();

export default {
    namespaced: true,
    state: {
        supplyId: null,
        supplies: [],
        supply: null
    },
    mutations: {
        setSupplyId(state, supplyId) {
            state.supplyId = supplyId
        },
        setSupply(state, supply) {
            state.supply = supply
        },
        setSupplies(state, supplies) {
            state.supplies = supplies
        },
        appendSupply(state, supply) {
            state.supplies.push(supply)
        },
        replaceSupply(state, supply) {
            let indexFounded = -1
            state.supplies.findIndex(function(element, index){
                if(element.id === supply.id){
                    indexFounded = index
                }
            })
            if (indexFounded > -1) {
                state.supplies.splice(indexFounded, 1, supply);
            }
        }
    },
    actions: {
        getSupplies({commit}) {
            return apiService.getSupplies()
                            .then(({data}) => {
                                commit('setSupplies', data)
                            })
        },
        createSupply({commit, state}) {
            return apiService.createSupply(state.supply)
                            .then( ({data}) => {
                                commit('appendSupply', data)
                                commit('setSupply', null)
                            })
        },
        updateSupply({commit, state}) {
            return apiService.updateSupply(state.supplyId, state.supply)
                            .then( ({data}) => {                                
                                commit('replaceSupply', data)
                                commit('setSupply', null)
                                commit('setSupplyId', null)
                            })
        },
        inactivateSupply({commit, state}) {
            return apiService.inactivateSupply(state.supplyId)
                            .then( ({data}) => {                                
                                commit('replaceSupply', data)
                                commit('setSupply', null)
                                commit('setSupplyId', null)
                            })
        }
    }

}