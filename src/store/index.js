import createPersistedState from 'vuex-persistedstate'

import Vue from 'vue'
import Vuex from 'vuex'
import authentication from './modules/authentication'
import roles from './modules/roles'
import supplies from './modules/supplies'
import assets from './modules/assets'
import sales from './modules/sales'
import orders from './modules/orders'
import payment from './modules/payment'
import production from './modules/production'
import debts from './modules/debts'
import provider from './modules/providers'
import storehouses from './modules/storehouses'
import cash from './modules/cash';
import mobilities from './modules/mobilities';
import customers from './modules/customers';
import notifications from './modules/notifications';
import reports from './modules/reports';
import products from './modules/products';
import drums from './modules/drums';
import DetailPayments from './modules/detail-payments';
import takings from './modules/takings';

Vue.use(Vuex)

export default new Vuex.Store({
    strict: true,
    state: {

    },
    modules: {
        authentication,
        customers,
        roles,
        supplies,
        assets,
        sales,
        mobilities,
        orders,
        payment,
        production,
        products,
        debts,
        provider,
        storehouses,
        cash,
        notifications,
        reports,
        drums,
        "detail-payments": DetailPayments,
        takings
    },
    mutations: {

    },
    actions: {

    },
    plugins: [
        createPersistedState(),
    ]
})