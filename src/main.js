import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/es';
import axios from 'axios';
import VueAxios from 'vue-axios'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueSweetalert2 from 'vue-sweetalert2'
import Vue2Filters from 'vue2-filters'
import store from './store/index'
import router from './router/index'
import permission from './plugins/vue-permission';
import Notifications from 'vue-notification'
import HighchartsVue from "highcharts-vue";

import { sync } from 'vuex-router-sync'

//---CSS
import "@/assets/css/main.css";
import "@/assets/css/crud.css";
import "@/assets/css/stadistic.css";
import "@/assets/css/status.css";
import "@/assets/css/header.css";
import "@/assets/css/custom-form.css";
import "@/assets/css/colors.css";
import "@/assets/css/spaces.css";
import "@/assets/css/sales.css";
import "@/assets/fontawesome/css/all.css";
import 'sweetalert2/dist/sweetalert2.min.css';
import "@/assets/css/voucher.css";

import  "@/helpers/Filters";

Vue.config.productionTip = false
Vue.use(ElementUI, { locale });
Vue.use(VueAxios, axios)
Vue.use(VueSweetalert2)
Vue.use(Vue2Filters)
Vue.use(permission)
Vue.use(Notifications)
Vue.use(HighchartsVue)

sync(store, router)

//let moment = require('moment');
const moment = require('moment-timezone');
moment.tz.setDefault("America/Lima");
require('moment/locale/es')

Vue.use(require('vue-moment'), {
  moment
})

//global variable
Vue.prototype.$token = ''

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
