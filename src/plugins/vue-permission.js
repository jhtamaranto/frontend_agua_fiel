import store from '@/store'

export default {
    install(Vue, options) {
        Vue.prototype.$hasPermision = function(permision) {
            let has = store.getters && store.getters['authentication/hasPermission'](permision)

            return has
        }

        Vue.prototype.$identifiedUser = function() {
            return store.getters['authentication/getIdentifiedUser']
        }

        Vue.prototype.$userRoles = function() {
            return store.getters['authentication/getUserRoles']
        }

        Vue.prototype.$isDeliveryMan = function() {
            let role = store.getters['authentication/getUserRoles']
            let is_delivery = false
            if (role[0].toLowerCase() == 'repartidor') {
                is_delivery = true
            }
            return is_delivery
        }
    }
}