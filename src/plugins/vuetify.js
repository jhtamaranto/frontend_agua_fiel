import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors'
import { Ripple } from 'vuetify/lib/directives';

//Vue.use(Vuetify);

Vue.use(Vuetify, {
    directives: {
      Ripple
    }
  })

export default new Vuetify({
    
});
