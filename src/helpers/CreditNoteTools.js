import Vue from 'vue';

export default {
    generateVoucherSale (creditNote, sale, companyData) {
        let itemsProducts = ``;
        let discount_total = 0;
        let totalRows = 20;

        let rowsWithData = 0;
        sale.detail.forEach(el => {
          rowsWithData++;
            let quantity = el.quantity.toString().padStart(3, '0');
            
            let name = el.name;
            let value_subtotal = parseFloat(el.subtotal);
            let subtotal_item = value_subtotal;
            let subtotal = `S/. ${parseFloat(subtotal_item).toFixed(2)}`.padStart(30, ' ');
            discount_total = discount_total + parseFloat(el.discount_item);

            itemsProducts += `<tr>
              <td class="borde-derecha v-letra-9">${name.toUpperCase()}</td>
              <td class="texto-derecha borde-derecha v-letra-9">${quantity}</td>
              <td class="texto-centro borde-derecha v-letra-9">NIU</td>
              <td class="texto-derecha borde-derecha v-letra-9">${parseFloat(el.base_price).toFixed(2)}</td>
              <td class="texto-derecha v-letra-9">${parseFloat(el.subtotal_original).toFixed(2)}</td>
            </tr>`;
        });

        let rowsEmpty = totalRows - rowsWithData;
        if (rowsEmpty > 0) {
          for (let row = 0; row < rowsEmpty; row++) {
            itemsProducts += `<tr>
            <td class="borde-derecha v-letra-9"></td>
            <td class="borde-derecha v-letra-9"></td>
            <td class="borde-derecha v-letra-9"></td>
            <td class="borde-derecha v-letra-9"></td>
            <td></td>
          </tr>`;
          }
        }
        

        let company_email = '';
        if (companyData.company_email) {
          company_email = `<span>${companyData.company_email}</span>`;
        }

        let company_phone = '';
        let company_phone_label = '';
        if (companyData.company_phone) {
          company_phone = companyData.company_phone;
          if (companyData.phone_aux) {
            company_phone = company_phone + ' / ' + companyData.phone_aux;
          }
          company_phone_label = `<span>${company_phone}</span>`;
        }

        let type_document = 'NOTA DE CRÉDITO ELECTRÓNICA';

        let type_document_modified = sale.type_document;

        let document_customer = '';
        if (sale.customer.document) {
            document_customer = `<span>${sale.customer.document}</span>`;
        }

        let customer_name = sale.customer.name.toUpperCase();
        if (sale.customer.type == 'Natural') {            
            if (sale.customer.surname){
              customer_name = customer_name + ' ' + sale.customer.surname.toUpperCase();
            }
        }
        let customer_address = '';
        if (sale.customer.address) {
          customer_address = sale.customer.address;
        }
        if (sale.order_address) {
          customer_address = sale.order_address;
        }

        let seller_document = '';
        let seller_name = '';
        if (sale.user) {
            seller_name = sale.user.first_name.toUpperCase();
            if (sale.user.last_name) {
                seller_name = seller_name + ' ' + sale.user.last_name.toUpperCase();
            }
        }

        let method_payment = '';
        if (sale.type_sale) {
          method_payment = sale.type_sale.toUpperCase();
        }

        let content = `
            <style>
              @import url('https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,400;0,700;1,400;1,700&display=swap');

              @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap');

              .voucher-content{
                font-family: 'Open Sans', sans-serif !important;
                font-size: 9px !important;
              }

              .v-letra-9{
                font-family: 'Open Sans', sans-serif !important;
                font-size: 9px !important;
              }

              .voucher-header{
                display: table;
                width: 100%
              }

              .voucher-header-row{
                display: table-row;
              }

              .voucher-header-cell{
                display: table-cell;
                vertical-align: top;
              }

              .columna-logo {
                width: 60%;
              }

              .columna-numero{
                width: 40%
              }

              .texto-negrita{
                font-weight: bold;
              }

              .texto-rojo{
                color: #E53935;
              }

              .voucher-number {
                border: 1px solid;
                padding-left: 20px;
                padding-right: 20px;
                font-size: 20px;
                text-align: center;
                padding-top: 15px;
                padding-bottom: 15px;
                border-radius: 3px;
              }

              .voucher-customer{
                padding-top: 10px;
              }

              .voucher-customer-data{
                border: 1px solid;
                border-radius: 3px;
              }

              .voucher-detail-data{
                border: 1px solid;
                border-radius: 3px;
              }

              .voucher-detail{
                padding-top: 15px;
              }

              .borde-inferior{
                border-bottom: 1px solid;
              }

              .borde-derecha{
                border-right: 1px solid;
              }

              .texto-derecha{
                text-align: right;
              }

              .texto-centro{
                text-align: center;
              }

              .all-borders{
                border: 1px solid;
              }

              .borde-izquierdo{
                border-left: 1px solid;
              }

              .borde-arriba{
                border-top: 1px solid,
              }

              .borde-abajo{
                border-bottom: 1px solid;
              }
            </style>

            <div class="voucher-content">
              <div class="voucher-header">
                <div class="voucher-header-row">
                  <div class="voucher-header-cell columna-logo">
                    <div class="voucher-logo">
                      <img src="/images/logo-chico.png" width="180px" height="50px">		
                    </div>
                    <div>
                      <table>
                        <tbody>
                          <tr>
                            <td><span class="v-letra-9">${companyData.company_name.toUpperCase()}</span></td>
                          </tr>
                          <tr>
                            <td><span class="v-letra-9">${companyData.company_address}</span></td>
                          </tr>
                          <tr>
                            <td class="v-letra-9">${company_email}</td>
                          </tr>
                          <tr>
                            <td class="v-letra-9">${company_phone_label}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="voucher-header-cell columna-numero">
                    <div class="voucher-number">
                      <span class="texto-negrita">
                        R.U.C N° ${companyData.company_document}
                      </span>
                      <br>
                      <span class="texto-negrita">${type_document}</span>
                      <br>
                      <span class="texto-negrita">N°</span>
                      <span class="texto-negrita texto-rojo">${creditNote.serie}-${creditNote.correlative}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="voucher-customer">
                <div class="voucher-customer-data">
                  <table cellpadding="5px" cellspacing="3px" style="width: 100%;">
                    <tbody>
                      <tr>
                        <td style="width: 10%;" class="v-letra-9"><b>SR. (ES):</b></td>
                        <td style="width: 45%;" class="v-letra-9"><span>${customer_name}</span></td>
                        <td style="width: 25%;" class="v-letra-9"><b>FECHA DE EMISIÓN:</b></td>
                        <td style="width: 20%;" class="v-letra-9">
                          <span>${Vue.prototype.$moment(creditNote.created_at).format('DD/MM/YYYY')}</span>
                        </td>
                      </tr>
                      <tr>
                        <td style="width: 10%;" class="v-letra-9"><b>${sale.customer.type_document}:</b></td>
                        <td style="width: 45%;" class="v-letra-9"><span>${document_customer}</span></td>
                        <td style="width: 25%;" class="v-letra-9"><b>VENDEDOR:</b></td>
                        <td style="width: 20%;" class="v-letra-9"><span>${seller_name}</span></td>
                      </tr>
                      <tr>
                        <td style="width: 10%;" class="v-letra-9"><b>DIRECCION:</b></td>
                        <td style="width: 45%;" class="v-letra-9"><span>${customer_address}</span></td>
                        <td style="width: 25%;" class="v-letra-9"><b>TIPO DE MONEDA:</b></td>
                        <td style="width: 20%;" class="v-letra-9">SOLES</td>
                      </tr>
                      <tr>
                        <td style="width: 10%;" class="v-letra-9">&nbsp;</td>
                        <td style="width: 45%;" class="v-letra-9">&nbsp;</td>
                        <td style="width: 25%;" class="v-letra-9"><b>MÉTODO DE PAGO:</b></td>
                        <td style="width: 20%;" class="v-letra-9"><span>${method_payment}</span></td>
                      </tr>
                    </tbody>					
                  </table>
                </div>
              </div>
              <div class="voucher-customer">
                <div class="voucher-customer-data">
                  <table cellpadding="5px" cellspacing="3px" style="width: 100%;">
                    <tbody>
                      <tr>
                        <td style="width: 25%;" class="v-letra-9"><b>DOCUMENTO QUE MODIFICA:</b></td>
                        <td style="width: 25%;" class="v-letra-9"><span>${type_document_modified}</span></td>
                        <td style="width: 25%;" class="v-letra-9"><b>FECHA EMISIÓN QUE MODIFICA:</b></td>
                        <td style="width: 25%;" class="v-letra-9">
                          <span>${Vue.prototype.$moment(sale.date_invoice).format('DD/MM/YYYY')}</span>
                        </td>
                      </tr>
                      <tr>
                        <td style="width: 25%;" class="v-letra-9"><b>SERIE Y NÚMERO QUE MODIFICA:</b></td>
                        <td style="width: 25%;" class="v-letra-9"><span>${sale.serie}-${sale.correlative}</span></td>
                        <td style="width: 25%;" class="v-letra-9"><b>MOTIVO:</b></td>
                        <td style="width: 25%;" class="v-letra-9"><span>ANULACIÓN DE LA OPERACIÓN</span></td>
                      </tr>
                    </tbody>					
                  </table>
                </div>
              </div>
              <div class="voucher-detail">
                <div class="voucher-detail-data">
                  <table style="width: 100%; border-collapse:collapse;" cellpadding="5px">
                    <thead>
                      <tr>
                        <th class="borde-inferior borde-derecha v-letra-9">DESCRIPCION</th>
                        <th class="borde-inferior borde-derecha v-letra-9">CANTIDAD</th>
                        <th class="borde-inferior borde-derecha v-letra-9">UND.</th>
                        <th class="borde-inferior borde-derecha v-letra-9">PRECIO UNITARIO</th>
                        <th class="borde-inferior v-letra-9">PRECIO VENTA</th>
                      </tr>
                    </thead>
                    <tbody>
                      ${itemsProducts}
                    </tbody>
                    <tfoot>
                      <tr>
                        <td rowspan="6" class="all-borders v-letra-9" colspan="3" style="border-left: none !important;">
                        </td>
                        <td class="all-borders v-letra-9">
                          <span>OP. GARAVADAS</span>
                        </td>
                        <td class="all-borders texto-derecha v-letra-9">
                          <span>S/ ${parseFloat(sale.subtotal).toFixed(2)}</span>
                        </td>
                      </tr>
                      <tr>
                        <td class="all-borders v-letra-9">
                          <span>OP. INAFECTAS</span>
                        </td>
                        <td class="all-borders texto-derecha v-letra-9">
                          <span>S/ 0.00</span>
                        </td>
                      </tr>
                      <tr>
                        <td class="all-borders v-letra-9">
                          <span>OP. EXONERADAS</span>
                        </td>
                        <td class="all-borders texto-derecha v-letra-9">
                          <span>S/ 0.00</span>
                        </td>
                      </tr>
                      <tr>
                        <td class="all-borders v-letra-9">
                          <span>OP. GRATUITAS</span>
                        </td>
                        <td class="all-borders texto-derecha v-letra-9">
                          <span>S/ ${parseFloat(sale.total_gratuitas).toFixed(2)}</span>
                        </td>
                      </tr>
                      <tr>
                        <td class="all-borders v-letra-9">
                          <span>OTROS CARGOS</span>
                        </td>
                        <td class="all-borders texto-derecha v-letra-9">
                          <span>S/ 0.00</span>
                        </td>
                      </tr>
                      <tr>
                        <td class="all-borders v-letra-9">
                          <span>OTROS TRIBUTOS</span>
                        </td>
                        <td class="all-borders texto-derecha v-letra-9">
                          <span>S/ 0.00</span>
                        </td>
                      </tr>
                      <tr>
                        <td rowspan="3" class="borde-arriba v-letra-9" colspan="3">
                          <span>
                            <i>
                              SON: ${this.NumerosaLetras(parseFloat(sale.total).toFixed(2)).toUpperCase()} SOLES
                            </i>
                          </span>
                        </td>
                        <td class="all-borders v-letra-9">
                          <span>DESCUENTO</span>
                        </td>
                        <td class="all-borders texto-derecha v-letra-9">
                          <span>S/ ${parseFloat(discount_total).toFixed(2)}</span>
                        </td>
                      </tr>
                      <tr>
                        <td class="all-borders v-letra-9"><span>I.G.V. 18%</span></td>
                        <td class="all-borders texto-derecha v-letra-9"><span>S/ ${parseFloat(sale.igv).toFixed(2)}</span></td>
                      </tr>
                      <tr>
                        <td class="borde-izquierdo v-letra-9"><span>TOTAL</span></td>
                        <td class="borde-izquierdo texto-derecha v-letra-9"><span>S/ ${parseFloat(sale.total).toFixed(2)}</span></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
              <div class="voucher-footer">
                <table>
                  <tbody>
                    <tr>
                      <td style="vertical-align: bottom; padding-bottom: 20px;">
                        <span class="v-letra-9">Representación impresa de la Factura Electrónica, autorizado mediante Resolución de Intendencia N.° 0180050001789.</span>
                      </td>
                      <td style="vertical-align: bottom;">
                        <div id="qrcode">
                          <img src="/images/qr-senda.png" width="120px" height="120px" />
                          <br/>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>`;        

        return content;
    },

    NumerosaLetras (cantidad) {
        var numero = 0;
        cantidad = parseFloat(cantidad);
      
        if (cantidad == "0.00" || cantidad == "0") {
          return "CERO con 00/100 ";
        } else {
          var ent = cantidad.toString().split(".");
          var arreglo = this.separar_split(ent[0]);
          var longitud = arreglo.length;
      
          switch (longitud) {
            case 1:
              numero = this.unidades(arreglo[0]);
              break;
            case 2:
              numero = this.decenas(arreglo[0], arreglo[1]);
              break;
            case 3:
              numero = this.centenas(arreglo[0], arreglo[1], arreglo[2]);
              break;
            case 4:
              numero = this.unidadesdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3]);
              break;
            case 5:
              numero = this.decenasdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3], arreglo[4]);
              break;
            case 6:
              numero = this.centenasdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3], arreglo[4], arreglo[5]);
              break;
          }
      
          ent[1] = isNaN(ent[1]) ? '00' : ent[1];
      
          return numero.trim() + " con " + ent[1] + "/100";
        }
      },
      
      unidades(unidad) {
        let unidades = Array('UN ','DOS ','TRES ' ,'CUATRO ','CINCO ','SEIS ','SIETE ','OCHO ','NUEVE ');
      
        return unidades[unidad - 1];
      },
      
      decenas(decena, unidad) {
        let diez = Array('ONCE ','DOCE ','TRECE ','CATORCE ','QUINCE' ,'DIECISEIS ','DIECISIETE ','DIECIOCHO ','DIECINUEVE ');
        let decenas = Array('DIEZ ','VEINTE ','TREINTA ','CUARENTA ','CINCUENTA ','SESENTA ','SETENTA ','OCHENTA ','NOVENTA ');
      
        if (decena ==0 && unidad == 0) {
          return "";
        }
      
        if (decena == 0 && unidad > 0) {
          return this.unidades(unidad);
        }
      
        if (decena == 1) {
          if (unidad == 0) {
            return decenas[decena -1];
          } else {
            return diez[unidad -1];
          }
        } else if (decena == 2) {
          if (unidad == 0) {
            return decenas[decena -1];
          }
          else if (unidad == 1) {
            let veinte = '';
            return veinte = "VEINTI" + "UNO";
          }
          else {
            let veinte = '';
            return veinte = "VEINTI" + this.unidades(unidad);
          }
        } else {
      
          if (unidad == 0) {
            return decenas[decena -1] + " ";
          }
          if (unidad == 1) {
            return decenas[decena -1] + " Y " + "UNO";
          }
      
          return decenas[decena -1] + " Y " + this.unidades(unidad);
        }
      },
      
      centenas(centena, decena, unidad) {
        let centenas = Array( "CIENTO ", "DOSCIENTOS ", "TRESCIENTOS ", "CUATROCIENTOS ","QUINIENTOS ","SEISCIENTOS ","SETECIENTOS ", "OCHOCIENTOS ","NOVECIENTOS ");
      
        if (centena == 0 && decena == 0 && unidad == 0) {
          return "";
        }
        if (centena == 1 && decena == 0 && unidad == 0) {
          return "CIEN ";
        }
      
        if (centena == 0 && decena == 0 && unidad > 0) {
          return this.unidades(unidad);
        }
      
        if (decena == 0 && unidad == 0) {
          return centenas[centena - 1]  +  "" ;
        }
      
        if (decena == 0) {
          var numero = centenas[centena - 1] + "" + this.decenas(decena, unidad);
          return numero.replace(" Y ", " ");
        }
        if (centena == 0) {
      
          return  this.decenas(decena, unidad);
        }
      
        return centenas[centena - 1]  +  "" + this.decenas(decena, unidad);
      
      },
      
      unidadesdemillar(unimill, centena, decena, unidad) {
        let numero = this.unidades(unimill) + " MIL " + this.centenas(centena, decena, unidad);
        numero = numero.replace("UN  MIL ", "MIL " );
        if (unidad == 0) {
          return numero.replace(" Y ", " ");
        } else {
          return numero;
        }
      },
      
      decenasdemillar(decemill, unimill, centena, decena, unidad) {
        let numero = this.decenas(decemill, unimill) + " MIL " + this.centenas(centena, decena, unidad);
        return numero;
      },
      
      centenasdemillar(centenamill,decemill, unimill, centena, decena, unidad) {
      
        let numero = 0;
        numero = this.centenas(centenamill,decemill, unimill) + " MIL " + this.centenas(centena, decena, unidad);
      
        return numero;
      },
      
      separar_split(texto){
        let contenido = new Array();
        for (var i = 0; i < texto.length; i++) {
          contenido[i] = texto.substr(i,1);
        }
        return contenido;
      }
}