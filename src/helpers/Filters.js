import Vue from 'vue'

import Tools from './Tools'

Vue.filter('numberOrder', function(value) {
    return Tools.generateNumberOrder(value) 
})
