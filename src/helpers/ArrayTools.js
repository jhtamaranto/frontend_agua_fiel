export default {
    getIndexById(data, search) {
        let indexFounded = -1
        data.forEach( (element, index) => {
            if (element.id === search) {
                indexFounded = index
            }
        })

        return indexFounded
    },
}