import Vue from 'vue';

export default {
    generateVoucherSale (sale, companyData) {
        let itemsProducts = ``;

        sale.detail.forEach(el => {
            let quantity = el.quantity.toString().padStart(3, '0');
            let description = '';
            if (el.description) {
                description = ' - ' + el.description;
            }
            let name = el.name + description;
            let value_subtotal = parseFloat(el.subtotal);
            //let value_discount = parseFloat(el.discount_value);
            let subtotal_item = value_subtotal;
            let subtotal = `S/. ${parseFloat(subtotal_item).toFixed(2)}`.padStart(30, ' ')
            
            itemsProducts += `<tr>
                <td style="text-align: left; vertical-align: top;"><b>[${quantity}]</b></td>
                <td style="text-align: left; vertical-align: top;">${name}</td>
                <td style="text-align: right; vertical-align: top;">${parseFloat(el.base_price).toFixed(2)}</td>
                <td style="text-align: right; vertical-align: top;">${subtotal}</td>
            </tr>`;
        });

        let company_email = '';
        if (companyData.company_email) {
          company_email = `<span class="voucher-label">${companyData.company_email}</span><br>`;
        }

        let company_phone = '';
        let company_phone_label = '';
        if (companyData.company_phone) {
          company_phone = companyData.company_phone;
          if (companyData.phone_aux) {
            company_phone = company_phone + ' / ' + companyData.phone_aux;
          }
          company_phone_label = `<span class="voucher-label">${company_phone}</span><br>`;
        }

        let type_document = 'FACTURA ELECTRÓNICA';
        if (sale.type_document === 'BOLETA') {
            type_document = 'BOLETA DE VENTA ELECTRÓNICA';
        }

        let document_customer = '';
        if (sale.customer.document) {
            document_customer = `<span class="voucher-label"><b>${sale.customer.type_document}: </b>${sale.customer.document}</span><br>`;
        }

        let customer_name = sale.customer.name.toUpperCase();
        if (sale.customer.type == 'Natural') {            
            if (sale.customer.surname){
              customer_name = customer_name + ' ' + sale.customer.surname.toUpperCase();
            }
        }

        let seller_document = '';
        let seller_name = '';
        if (sale.user) {
            seller_name = sale.user.first_name;
            if (sale.user.last_name) {
                seller_name = seller_name + ' ' + sale.user.last_name;
            }
        }

        let content = `
        <style type="text/css">
            @import url('https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,400;0,700;1,400;1,700&display=swap');
        
            @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap');

            .voucher-content {
                width: 100%;
                font-family: 'Open Sans', sans-serif;
                font-size: 11px;
                margin-left: 0px;
                margin-right: 0px;
            }

            .voucher-label{
                margin-top: 20px !important;
            }
        </style>        
        <div class='voucher-content'>
            <div style="text-align: center">
                <img class="imgCenter" src='/images/logo-1.png' height="140px" width="180px" /> 
                <br>
                <span class="voucher-label"><b>${companyData.company_name.toUpperCase()}</b></span><br>
                <span class="voucher-label"><b>${companyData.company_document}</b></span><br>
                <span class="voucher-label">${companyData.company_address}</span><br>
                ${company_email}
                ${company_phone_label}             
                <br>
                <br>
                <span class="voucher-label"><b>${type_document}</b></span><br>
                <span class="voucher-label"><b>${sale.serie}-${sale.correlative}</b></span><br><br>
            </div>
            <div>
                <span class="voucher-label"><b>ADQUIRIENTE</b></span><br>
                ${document_customer}
                <span class="voucher-label">${customer_name}</span><br>
                <span class="voucher-label"><b>FECHA EMISION: </b>${Vue.prototype.$moment(sale.entry_time).format('DD/MM/YYYY')}</span><br>
                <span class="voucher-label"><b>HORA EMISION: </b>${Vue.prototype.$moment(sale.entry_time).format('HH:mm:ss a')}</span><br>            
                <span class="voucher-label"><b>MONEDA: </b>SOLES PERUANOS</span><br>
                <span class="voucher-label"><b>IGV: </b>18.00</span><br>
                <span class="voucher-label"><b>VENDEDOR:</b> ${seller_name.toUpperCase()}</span>
            </div>
            <div>
                <table style="width: 100%; border-top: 2px solid; border-bottom: 2px solid;font-size: 11px;">
                    <thead>
                        <tr>
                            <th style="text-align: left; width: 10%;">[CANT]</th>
                            <th style="text-align: left; width: 35%;">DESCRIPCION</th>
                            <th style="text-align: right;width: 25%;">P/U</th>
                            <th style="text-align: right;width: 30%;">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${itemsProducts}
                    </tbody>
                </table>
            </div>
            <div>
                <table style="width: 100%;font-size: 11px;border-bottom: 2px solid;">
                    <tbody>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>OP. GRAVADAS</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ ${parseFloat(sale.subtotal).toFixed(2)}</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>OP. GRATUITAS</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ 0.00</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>OP. EXONERADAS</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ 0.00</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>OP. INAFECTA</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ 0.00</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>TOT. DESCUENTO GLOBAL</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ ${parseFloat(sale.discount).toFixed(2)}</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>I.S.C</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ 0.00</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>I.G.V (18%)</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ ${parseFloat(sale.igv).toFixed(2)}</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>OTROS TRIBUTOS</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ 0.00</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>REDONDEO</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ 0.01</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 70%; text-align: left;">
                                <span class="voucher-label"><b>TOTAL VENTA</b></span>
                            </td>
                            <td style="width: 30%; text-align: right;">
                                <span class="voucher-label"><b>S/ ${parseFloat(sale.total).toFixed(2)}</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br><br>
                                <span><i>SON: ${this.NumerosaLetras(parseFloat(sale.total).toFixed(2)).toUpperCase()} SOLES</i></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="text-align: center">
              <p>
                Representación impresa del documento electrónico, esta puede ser consultado en:
                <br>
                https://sendatisolutions.com/
                <br>
                <br>
                <br>
              </p>
            </div>
        </div>`;

        return content;
    },

    NumerosaLetras (cantidad) {
        var numero = 0;
        cantidad = parseFloat(cantidad);
      
        if (cantidad == "0.00" || cantidad == "0") {
          return "CERO con 00/100 ";
        } else {
          var ent = cantidad.toString().split(".");
          var arreglo = this.separar_split(ent[0]);
          var longitud = arreglo.length;
      
          switch (longitud) {
            case 1:
              numero = this.unidades(arreglo[0]);
              break;
            case 2:
              numero = this.decenas(arreglo[0], arreglo[1]);
              break;
            case 3:
              numero = this.centenas(arreglo[0], arreglo[1], arreglo[2]);
              break;
            case 4:
              numero = this.unidadesdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3]);
              break;
            case 5:
              numero = this.decenasdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3], arreglo[4]);
              break;
            case 6:
              numero = this.centenasdemillar(arreglo[0], arreglo[1], arreglo[2], arreglo[3], arreglo[4], arreglo[5]);
              break;
          }
      
          ent[1] = isNaN(ent[1]) ? '00' : ent[1];
      
          return numero.trim() + " con " + ent[1] + "/100";
        }
      },
      
      unidades(unidad) {
        let unidades = Array('UN ','DOS ','TRES ' ,'CUATRO ','CINCO ','SEIS ','SIETE ','OCHO ','NUEVE ');
      
        return unidades[unidad - 1];
      },
      
      decenas(decena, unidad) {
        let diez = Array('ONCE ','DOCE ','TRECE ','CATORCE ','QUINCE' ,'DIECISEIS ','DIECISIETE ','DIECIOCHO ','DIECINUEVE ');
        let decenas = Array('DIEZ ','VEINTE ','TREINTA ','CUARENTA ','CINCUENTA ','SESENTA ','SETENTA ','OCHENTA ','NOVENTA ');
      
        if (decena ==0 && unidad == 0) {
          return "";
        }
      
        if (decena == 0 && unidad > 0) {
          return this.unidades(unidad);
        }
      
        if (decena == 1) {
          if (unidad == 0) {
            return decenas[decena -1];
          } else {
            return diez[unidad -1];
          }
        } else if (decena == 2) {
          if (unidad == 0) {
            return decenas[decena -1];
          }
          else if (unidad == 1) {
            let veinte = '';
            return veinte = "VEINTI" + "UNO";
          }
          else {
            let veinte = '';
            return veinte = "VEINTI" + this.unidades(unidad);
          }
        } else {
      
          if (unidad == 0) {
            return decenas[decena -1] + " ";
          }
          if (unidad == 1) {
            return decenas[decena -1] + " Y " + "UNO";
          }
      
          return decenas[decena -1] + " Y " + this.unidades(unidad);
        }
      },
      
      centenas(centena, decena, unidad) {
        let centenas = Array( "CIENTO ", "DOSCIENTOS ", "TRESCIENTOS ", "CUATROCIENTOS ","QUINIENTOS ","SEISCIENTOS ","SETECIENTOS ", "OCHOCIENTOS ","NOVECIENTOS ");
      
        if (centena == 0 && decena == 0 && unidad == 0) {
          return "";
        }
        if (centena == 1 && decena == 0 && unidad == 0) {
          return "CIEN ";
        }
      
        if (centena == 0 && decena == 0 && unidad > 0) {
          return this.unidades(unidad);
        }
      
        if (decena == 0 && unidad == 0) {
          return centenas[centena - 1]  +  "" ;
        }
      
        if (decena == 0) {
          var numero = centenas[centena - 1] + "" + this.decenas(decena, unidad);
          return numero.replace(" Y ", " ");
        }
        if (centena == 0) {
      
          return  this.decenas(decena, unidad);
        }
      
        return centenas[centena - 1]  +  "" + this.decenas(decena, unidad);
      
      },
      
      unidadesdemillar(unimill, centena, decena, unidad) {
        let numero = this.unidades(unimill) + " MIL " + this.centenas(centena, decena, unidad);
        numero = numero.replace("UN  MIL ", "MIL " );
        if (unidad == 0) {
          return numero.replace(" Y ", " ");
        } else {
          return numero;
        }
      },
      
      decenasdemillar(decemill, unimill, centena, decena, unidad) {
        let numero = this.decenas(decemill, unimill) + " MIL " + this.centenas(centena, decena, unidad);
        return numero;
      },
      
      centenasdemillar(centenamill,decemill, unimill, centena, decena, unidad) {
      
        let numero = 0;
        numero = this.centenas(centenamill,decemill, unimill) + " MIL " + this.centenas(centena, decena, unidad);
      
        return numero;
      },
      
      separar_split(texto){
        let contenido = new Array();
        for (var i = 0; i < texto.length; i++) {
          contenido[i] = texto.substr(i,1);
        }
        return contenido;
      }
}