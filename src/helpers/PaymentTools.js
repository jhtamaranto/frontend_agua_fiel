import CartTools from './CartTools.js'

export default {
    prepareProductList(source, list) {
        let listProducts = []
        if (source === 'sales') {
            list.forEach(element => {
                let subtotal = CartTools.calculatePriceFromProductItem(element)
                let product = {
                    id: element.id,
                    name: element.name,
                    quantity: element.quantity,
                    base_price: element.base_price,
                    discount_amount: element.discount_amount,
                    subtotal: subtotal,
                    has_return: element.has_return,
                    discount_value: element.discount_value,
                    discount_group: element.discount_group,
                    discount_item: element.discount_item,
                    type_sale: element.type_sale,
                    set_discount_customer: element.set_discount_customer,
                    type_product: element.type_product,
                    unit_price: element.unit_price,
                    wholesale_price: element.wholesale_price,
                    preferential_price: element.preferential_price,
                    is_bonus: element.is_bonus
                }

                listProducts.push(product)
            })
        } else if (source === 'orders') {
            list.forEach(element => {
                let id = null;
                if (element.type_product == 'producto') {
                    id = 'p-' + element.product_id;
                } else if (element.type_product == 'activo') {
                    id = 'a-' + element.product_id;
                }
                let product = {
                    id: id,
                    name: element.name,
                    quantity: element.quantity,
                    base_price: element.base_price,
                    discount_amount: element.discount_amount,
                    subtotal: element.subtotal,
                    has_return: element.has_return,
                    discount_value: element.discount_value,
                    discount_group: element.discount_group,
                    discount_item: element.discount_item,
                    type_sale: element.type_sale,
                    set_discount_customer: element.set_discount_customer,
                    type_product: element.type_product,
                    unit_price: element.unit_price,
                    wholesale_price: element.wholesale_price,
                    preferential_price: element.preferential_price,
                    is_bonus: element.is_bonus
                }
                listProducts.push(product)
            });
        }
        return listProducts
    }
}