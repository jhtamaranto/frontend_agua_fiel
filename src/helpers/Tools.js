
export default {
    generateNumberOrder(numberOrder) {
        let numbreString = `${numberOrder}`
        const totalDigist = 6
        let newNumberOrder = numbreString
        for (let i = 0; i < (totalDigist - numbreString.length); i++) {
            newNumberOrder = '0' + newNumberOrder
        }
        return newNumberOrder
    },
    findIndexFromItem(data, idSearched) {
        let indexFounded = -1
        data.forEach( (element, index) => {
            if (element.id === idSearched) {
                indexFounded = index
            }
        })

        return indexFounded
    },
    getItemFromList(data, idSearched){
        let itemFounded = null
        data.forEach( (element, index) => {
            if (element.id === idSearched) {
                itemFounded = element
            }
        })

        return itemFounded
    },
    getNumberDecimalValue(number) {
        let value = 0
        if(!isNaN(number) && number !== null && number !== '') {
            value = parseFloat(number)
        }

        return value
    },
    getStatusProviderAccount(provider){
        let expiration = provider.date_agreement
        console.log(expiration)
        return 'Vencida'
    },
    getSumCash(data){
        let sum = 0
        data.forEach(element => {
            sum = sum + parseFloat(element.amount)
        })

        return sum
    },
    prepareCurrentDate(dateData = null) {
        if(dateData == null) {
            dateData = new Date();
        }
        let current = dateData;
        let year = current.getFullYear()
        let month = current.getMonth() + 1
        let day = current.getDate()

        if (month < 10) {
            month = '0' + month.toString()
        }

        if (day < 10) {
            day = '0' + day.toString()
        }

        let dateCurrent = year + '-' + month + '-' + day

        return dateCurrent
    },
    setFormatDate(dateData) {
        let dateFormatted = ''
        if (dateData != null) {
            let current = dateData;
            let year = current.getFullYear()
            let month = current.getMonth() + 1
            let day = current.getDate()

            if (month < 10) {
                month = '0' + month.toString()
            }

            if (day < 10) {
                day = '0' + day.toString()
            }

            dateFormatted = day + '/' + month + '/' + year
        }        

        return dateFormatted
    }
}