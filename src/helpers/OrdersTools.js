export default {
    getAvailableStatus(listStatus, currentStatus) {
        let availableStatus = []
        let options = []
        listStatus.forEach(element => {
            if (element.name.toLowerCase() === currentStatus.toLowerCase()) {
                if (element.options !== null) {
                    options = element.options.split(',')
                }
            }
        });

        if (options.length > 0) {
            listStatus.forEach(element => {
                if (options.includes(element.id.toString())) {
                    availableStatus.push(element)
                }
            })
        }

        return availableStatus
    },    
    filterOrdesForCustomerId(searchId, orders){
        let listFiltered = []
        orders.forEach( (element) => {
            if(element.customer.id === searchId) {
                listFiltered.push(element)
            }
        })

        return listFiltered
    }
}